import numpy as np
# <--------------------Functional coordinate transforms------------------------->
def cartesian_to_polar(x, y, degrees=False):
    """
    Converts arrays in 2D rectangular Cartesian coordinates to polar
    coordinates.
    
    :param x: First cartesian coordinate
    :type x: :class:`numpy.ndarray`
    :param y: Second cartesian coordinate
    :type y: :class:`numpy.ndarray`
    :param degrees: 
        If True, the output theta angle will be in degrees, otherwise radians.
    :type degrees: boolean
    
    :returns: 
        (r,theta) where theta is measured from the +x axis increasing towards
        the +y axis
    """
    r = (x * x + y * y) ** 0.5
    t = np.arctan2(y, x)
    if degrees:
        t = np.degrees(t)

    return r, t


def polar_to_cartesian(r, t, degrees=False):
    """
    Converts arrays in 2D polar coordinates to rectangular cartesian
    coordinates.
    
    Note that the spherical coordinates are in *physicist* convention such that
    (1,0,pi/2) is x-axis.
    
    :param r: Radial coordinate
    :type r: :class:`numpy.ndarray`
    :param t: Azimuthal angle from +x-axis increasing towards +y-axis
    :type t: :class:`numpy.ndarray`
    :param degrees: 
        If True, the input angles will be in degrees, otherwise radians.
    :type degrees: boolean
    
    :returns: arrays (x,y)
    """
    if degrees:
        t = np.radians(t)

    return r * np.cos(t), r * np.sin(t)


def cartesian_to_spherical(x, y, z, degrees=False):
    """
    Converts three arrays in 3D rectangular cartesian coordinates to
    spherical polar coordinates.
    
    Note that the spherical coordinates are in *physicist* convention such that
    (1,0,pi/2) is x-axis.
    
    :param x: First cartesian coordinate
    :type x: :class:`numpy.ndarray`
    :param y: Second cartesian coordinate
    :type y: :class:`numpy.ndarray`
    :param z: Third cartesian coordinate
    :type z: :class:`numpy.ndarray`
    :param degrees: 
        If True, the output theta angle will be in degrees, otherwise radians.
    :type degrees: boolean
    
    :returns: arrays (r,theta,phi) 
    """
    xsq, ysq, zsq = x * x, y * y, z * z
    r = (xsq + ysq + zsq) ** 0.5
    #t=np.arccos(z,r) #TODO:check to make even more efficient
    t = np.arctan2((xsq + ysq) ** 0.5, z)
    p = np.arctan2(y, x)
    if degrees:
        t, p = np.degrees(t), np.degrees(p)
    return r, t, p


def spherical_to_cartesian(r, t, p, degrees=False):
    """
    Converts arrays in 3D spherical polar coordinates to rectangular cartesian
    coordinates.
    
    Note that the spherical coordinates are in *physicist* convention such that
    (1,0,pi/2) is x-axis.
    
    :param r: Radial coordinate
    :type r: :class:`numpy.ndarray`
    :param t: Colatitude (angle from z-axis)
    :type t: :class:`numpy.ndarray`
    :param p: Azimuthal angle from +x-axis increasing towards +y-axis
    :type p: :class:`numpy.ndarray`
    :param degrees: 
        If True, the input angles will be in degrees, otherwise radians.
    :type degrees: boolean
    
    :returns: arrays (x,y,z)
    """
    if degrees:
        t, p = np.radians(t), np.radians(p)
    x = r * np.sin(t) * np.cos(p)
    y = r * np.sin(t) * np.sin(p)
    z = r * np.cos(t)

    return x, y, z


def latitude_to_colatitude(lat, degrees=False):
    """
    converts from latitude  (i.e. 0 at the equator) to colatitude/inclination 
    (i.e. "theta" in physicist convention).
    """
    if degrees:
        return 90 - lat
    else:
        return pi / 2 - lat


def colatitude_to_latitude(theta, degrees=False):
    """
    Converts from colatitude/inclination (i.e. "theta" in physicist convention) 
    to latitude (i.e. 0 at the equator).
    
    :param theta: input colatitude
    :type theta: float or array-like
    :param degrees: 
        If True, the input is interpreted as degrees, otherwise radians.
    :type degrees: bool
    
    :returns: latitude
    
    """
    if degrees:
        return 90 - theta
    else:
        return pi / 2 - theta


def cartesian_to_cylindrical(x, y, z, degrees=False):
    """
    Converts three arrays in 3D rectangular Cartesian coordinates to cylindrical
    polar coordinates.
    
    :param x: x cartesian coordinate
    :type x: float or array-like
    :param y: y cartesian coordinate
    :type y: float or array-like
    :param z: z cartesian coordinate
    :type z: float or array-like
    :param degrees: 
        If True, the output angles will be in degrees, otherwise radians.
    :type degrees: bool
    
    :returns: 
        Cylindrical coordinates as a (rho,theta,z) tuple (theta increasing from
        +x to +y, 0 at x-axis).
    """
    s, t = cartesian_to_polar(x, y)
    return s, t, z


def cylindrical_to_cartesian(s, t, z, degrees=False):
    """
    Converts three arrays in cylindrical polar coordinates to 3D rectangular
    Cartesian coordinates.
    
    :param s: radial polar coordinate
    :type s: float or array-like
    :param t: polar angle (increasing from +x to +y, 0 at x-axis)
    :type t: float or array-like
    :param z: z coordinate
    :type z: float or array-like
    :param degrees: 
        If True, the output angles will be in degrees, otherwise radians.
    :type degrees: bool
    
    :returns: Cartesian coordinates as an (x,y,z) tuple.
    """
    x, y = polar_to_cartesian(s, t, degrees)
    return x, y, z


def offset_proj_sep(rx, ty, pz, offset, spherical=False):
    """
    computes the projected separation for a list of points in galacto-centric
    coordinates as seen from a point offset (an [[x,y,z]] 2-sequence)
    
    spherical determines if the inputs are spherical coords or cartesian.  If it
    is 'degrees', spherical coordinates will be used, converting from degrees to
    radians
    """
    if spherical is 'degrees':
        x, y, z = spherical_to_cartesian(rx, ty, pz, True)
    elif spherical:
        x, y, z = spherical_to_cartesian(rx, ty, pz, False)
    else:
        x, y, z = rx, ty, pz

    offset = np.array(offset)
    if offset.shape[1] != 3 or len(offset.shape) != 2:
        raise ValueError('offset not a sequnce of 3-sequence')

    ohat = (offset.T * np.sum(offset * offset, 1) ** -0.5)
    return np.array(np.matrix(np.c_[x, y, z]) * np.matrix(ohat))


def sky_sep_to_3d_sep(pos1, pos2, d1, d2):
    """
    Compute the full 3D separation between two objects at distances `d1` and
    `d2` and angular positions `pos1` and `pos2`
    (:class:`~astropysics.coords.coordsys.LatLongCoordinates` objects, or an
    argument that will be used to generate a
    :class:`~astropysics.coords.coordsys.EquatorialCoordinatesEquinox` object)
    
    :param pos1: on-sky position of first object
    :type pos1: :class:`LatLongCoordinates` or initializer
    :param pos2: on-sky position of second object
    :type pos2: :class:`LatLongCoordinates` or initializer
    :param d1: distance to first object
    :type d1: scalar
    :param d2: distance to second object
    :type d2: scalar
    
    
    >>> from coordsys import LatLongCoordinates
    >>> p1 = LatLongCoordinates(0,0)
    >>> p2 = LatLongCoordinates(0,10)
    >>> '%.10f'%sky_sep_to_3d_sep(p1,p2,20,25)
    '6.3397355613'
    >>> '%.10f'%sky_sep_to_3d_sep('0h0m0s +0:0:0','10:20:30 +0:0:0',1,2)
    '2.9375007333'
        
    """
    from .coordsys import LatLongCoordinates, EquatorialCoordinatesEquinox

    if not isinstance(pos1, LatLongCoordinates):
        pos1 = EquatorialCoordinatesEquinox(pos1)
    if not isinstance(pos2, LatLongCoordinates):
        pos2 = EquatorialCoordinatesEquinox(pos2)

    return (pos1 - pos2).separation3d(d1, d2)


def radec_str_to_decimal(*args):
    """
    Convert a sequence of string coordinate specifiers to decimal degree arrays.
    
    Two input forms are accepted:
    
    * `radec_str_to_decimal(rastrs,decstrs)`
        In this form, `rastrs` and `decstrs` are sequences of strings with the
        RA and Dec, respectively.  
    * `radec_str_to_decimal(radecstrs)`
        In this form, `radecstrs` is a sequence of strings in any form accepted
        by the :class:`EquatorialCoordinatesBase` constructor. (typically
        canonical from like 17:43:54.23 +32:23:12.3)
    
    :returns: 
        (ras,decs) where `ras` and `decs` are  :class:`ndarrays <numpy.ndarray>`
        specifying the ra and dec in decimal degrees.
    
    """
    from .coordsys import AngularCoordinate, ICRSCoordinates
    from itertools import izip

    if len(args) == 1:
        for s in args[0]:
            c = ICRSCoordinates(s)
            ras.append(c.ra.d)
            decs.append(c.dec.d)
    elif len(args) == 2:
        ra, dec = args
        if len(ra) != len(dec):
            raise ValueError("length of ra and dec don't match")

        ras, decs = [], []
        for r, d in izip(ra, dec):
            ras.append(AngularCoordinate(r, sghms=True).d)
            decs.append(AngularCoordinate(d, sghms=False).d)
    else:
        raise ValueError('radec_str_to_decimal only accepts (rastr,decstr) or (radecstr)')

    return np.array(ras), np.array(decs)


def match_coords(a1, b1, a2, b2, eps=1, mode='mask'):
    """
    Match one pair of coordinate :class:`arrays <numpy.ndarray>` to another
    within a specified tolerance (`eps`).
    
    Distance is determined by the cartesian distance between the two arrays,
    implying the small-angle approximation if the input coordinates are
    spherical. Units are arbitrary, but should match between all coordinates
    (and `eps` should be in the same units)
    
    :param a1: the first coordinate for the first set of coordinates
    :type a1: array-like
    :param b1: the second coordinate for the first set of coordinates
    :type b1: array-like
    :param a2: the first coordinate for the second set of coordinates
    :type a2: array-like
    :param b2: the second coordinate for the second set of coordinates
    :type b2: array-like
    :param eps: 
        The maximum separation allowed for coordinate pairs to be considered
        matched.
    :type eps: float
    :param mode:
        Determines behavior if more than one coordinate pair matches.  Can be:
        
        * 'mask'
            Returns a 2-tuple of boolean arrays (mask1,mask2) where `mask1`
            matches the shape of the first coordinate set (`a1` and `b1`), and
            `mask2` matches second set (`a2` and `b2`). The mask value is True
            if a match is found for that coordinate pair, otherwise, False.
        * 'maskexcept' 
            Retuns the same values as 'mask', and will raise an exception if
            more than one match is found.
        * 'maskwarn'
            Retuns the same values as 'mask', and a warning will be issued if
            more than one match is found. 
        * 'count'
            Returns a 2-tuple (nmatch1,nmatch2) with the number of objects that
            matched for each of the two sets of coordinate systems.
        * 'index'
            Returns a 2-tuple of integer arrays (ind1,ind2). `ind1` is a set of
            indecies into the first coordinate set, and `ind2` indexes the 
            second.  The two arrays match in shape and each element is the 
            index for a matched pair of coordinates - e.g. a1[ind1[i]] and 
            a2[ind2[i]] will give the "a" coordinate for a matched pair
            of coordinates.
        * 'match2D'
            Returns a 2-dimensional bool array. The array element M[i,j] is True
            if the ith coordinate of the first coordinate set
            matches the jth coordinate of the second set.
        * 'nearest'
            Returns (nearestind,distance,match). `nearestind` is an int array
            such that nearestind holds indecies into the *second* set of
            coordinates for the nearest object to the ith object in the first
            coordinate set (hence, it's shape matches the *first* coordinate
            set). `distance` is a float array of the same shape giving the
            corresponding distance, and `match` is a boolean array that is True
            if the distance is within `eps`, and is the same shape as the other
            outputs. Note that if a1 and b1 are the same object (and a2 and b2),
            this finds the second-closest match (because the first will always
            be the object itself if the coordinate pairs are the same) This mode
            is a wrapper around :func:`match_nearest_coords`.
    
    :returns: See `mode` for a description of return types.
    
    **Examples**
    
    >>> from numpy import array
    >>> ra1 = array([1,2,3,4])
    >>> dec1 = array([0,0,0,0])
    >>> ra2 = array([4,3,2,1])
    >>> dec2 = array([3.5,2.5,1.5,.5])
    >>> match_coords(ra1,dec1,ra2,dec2,1)
    (array([ True, False, False, False], dtype=bool), array([False, False, False,  True], dtype=bool))
    """

    identical = a1 is a2 and b1 is b2

    a1 = np.array(a1, copy=False).ravel()
    b1 = np.array(b1, copy=False).ravel()
    a2 = np.array(a2, copy=False).ravel()
    b2 = np.array(b2, copy=False).ravel()

    #bypass the rest for 'nearest', as it calls match_nearest_coords
    if mode == 'nearest':
        #special casing so that match_nearest_coords dpes second nearest
        if identical:
            t = (a1, b1)
            seps, i2 = match_nearest_coords(t, t)
        else:
            seps, i2 = match_nearest_coords((a1, b1), (a2, b2))
        return i2, seps, (seps <= eps)

    def find_sep(A, B):
        At = np.tile(A, (len(B), 1))
        Bt = np.tile(B, (len(A), 1))
        return At.T - Bt

    sep1 = find_sep(a1, a2)
    sep2 = find_sep(b1, b2)
    sep = np.hypot(sep1, sep2)

    matches = sep <= eps

    if mode == 'mask':
        return np.any(matches, axis=1), np.any(matches, axis=0)
    elif mode == 'maskexcept':
        s1, s2 = np.sum(matches, axis=1), np.sum(matches, axis=0)
        if np.all(s1 < 2) and np.all(s2 < 2):
            return s1 > 0, s2 > 0
        else:
            raise ValueError('match_coords found multiple matches')
    elif mode == 'maskwarn':
        s1, s2 = np.sum(matches, axis=1), np.sum(matches, axis=0)
        from warnings import warn

        for i in np.where(s1 > 1)[0]:
            warn('1st index %i has %i matches!' % (i, s1[i]))
        for j in np.where(s2 > 1)[0]:
            warn('2nd index %i has %i matches!' % (j, s2[j]))
        return s1 > 0, s2 > 0
    elif mode == 'count':
        return np.sum(np.any(matches, axis=1)), np.sum(np.any(matches, axis=0))
    elif mode == 'index':
        return np.where(matches)
    elif mode == 'match2D':
        return matches.T
    elif mode == 'nearest':
        assert False, "'nearest' should always return above this - code should be unreachable!"
    else:
        raise ValueError('unrecognized mode')


def match_nearest_coords(c1, c2=None, n=None):
    """
    Match a set of coordinates to their nearest neighbor(s) in another set of
    coordinates.
    
    :param c1: 
        A D x N array with coordinate values (either as floats or
        :class:`AngularPosition` objects) or a sequence of
        :class:`LatLongCoordinates` objects for the first set of coordinates.
    :param c2: 
        A D x N array with coordinate values (either as floats or
        :class:`AngularPosition` objects) or a sequence of
        :class:`LatLongCoordinates` objects for the second set of coordinates.
        Alternatively, if this is None, `c2` will be set to `c1`, finding the 
        nearest neighbor of a point in `c1` to another point in `c1`.
    :param int n: 
        Specifies the nth nearest neighbor to be returned (1 means the closest
        match). If None, it will default to 2 if `c1` and `c2` are the same
        object (just equality is not enough - they must actually be the same
        in-memory array), or 1 otherwise. This is because if `c1` and `c2` are
        the same, a coordinate matches to *itself* instead of the nearest other
        coordinate.
    
    :returns: 
        (seps,ind2) where both are arrays matching the shape of `c1`. `ind2` is
        indecies into `c2` to find the nearest to the corresponding `c1`
        coordinate, and `seps` are the distances.
    """
    try:
        from scipy.spatial import cKDTree as KDTree
    except ImportError:
        from warnings import warn

        warn('C-based scipy kd-tree not available - match_nearest_coords will be much slower!')
        from scipy.spatial import KDTree

    if c2 is None:
        c2 = c1
    if n is None:
        n = 2 if c1 is c2 else 1

    c1 = np.array(c1, ndmin=1, copy=False)
    c2 = np.array(c2, ndmin=1, copy=False)

    if len(c1.shape) == 1:
        a1 = np.empty(c1.size)
        b1 = np.empty(c1.size)
        a2 = np.empty(c1.size)
        b2 = np.empty(c1.size)
        for i in range(len(c1)):
            a1[i] = c1[i].long.d
            b1[i] = c1[i].lat.d
            a2[i] = c2[i].long.d
            b2[i] = c2[i].lat.d
        c1 = np.array((a1, b1))
        c2 = np.array((a2, b2))
    elif len(c1.shape) != 2:
        raise ValueError('match_nearest_coords inputs have incorrect number of dimensions')

    if c1.shape[0] != c2.shape[0]:
        raise ValueError("match_nearest_coords inputs don't match in first dimension")

    kdt = KDTree(c2.T)
    if n == 1:
        return kdt.query(c1.T)
    else:
        dist, inds = kdt.query(c1.T, n)
        return dist[:, n - 1], inds[:, n - 1]


def separation_matrix(v, w=None, tri=False):
    """
    Computes a matrix of the separation between each of the components of the
    first dimension of an array. That is, A[i,j] = v[i]-w[j]. 
    
    :param v: The first array with first dimension n
    :param w: 
        The second array with first dimension m, and all following dimensions
        matched to `v`. If None, `v` will be treated as `w` (e.g. the separation
        matrix of `v` with itself will be returned).
    :param bool tri: 
        If True, the lower triangular part of the matrix is set to 0 (this is
        really only useful if w is None) 
        
    :returns: 
        Separation matrix with dimension nXmX(whatever the remaining dimensions
        are)
        
    .. seealso::
    
        :mod:`scipy.spatial.distance`, in particular the
        :func:`scipy.spatial.distance.pdist` function. It is much more efficient
        and flexible at computing distances if individual components and sign
        information is unnecessary.
        
    """
    if w is None:
        w = v

    shape1 = list(v.shape)
    shape1.insert(1, 1)
    shape2 = list(w.shape)
    shape2.insert(0, 1)

    A = v.reshape(shape1) - w.reshape(shape2)

    if tri:
        return np.triu(A)
    else:
        return A