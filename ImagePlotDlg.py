from sys import version_info
import logging

from PyQt4 import QtGui, QtCore
import pyqtgraph as pg
import numpy as np


class ImagePlotDlg(QtGui.QDialog):
    def __init__(self, radar, parent=None):
        super(ImagePlotDlg, self).__init__(parent)

        # initialse the logger
        self.logger = logging.getLogger(__name__)

        self.radar = radar

        # make sure the dialog is close (not hidden)
        # actually, I want to hide it
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        self.radioIm1 = QtGui.QRadioButton()
        self.radioIm1.setText("Wave Height")
        self.radioIm1.setChecked(True)
        self.radioIm2 = QtGui.QRadioButton()
        self.radioIm2.setText("Incident Radar Flux")
        self.radioIm3 = QtGui.QRadioButton()
        self.radioIm3.setText("Reflected Radar Flux")
        self.radioIm4 = QtGui.QRadioButton()
        self.radioIm4.setText("Is set")

        # stack = QtGui.QStackedWidget()

        win1 = pg.GraphicsLayoutWidget()
        self.image_view = win1.addViewBox()

        ## lock the aspect ratio so pixels are always square
        self.image_view.setAspectLocked(True)

        ## Create image item
        self.ccd_image = pg.ImageItem(border='w')
        self.image_view.addItem(self.ccd_image)

        self.image_view.setRange(QtCore.QRectF(0, 0, radar.camera.ccd_image.shape[0],
                                               radar.camera.ccd_image.shape[1]))

        # create the dialog with the tree and the buttons below it
        grid = QtGui.QGridLayout()
        grid.addWidget(win1, 0, 0, 1, 3)

        grid.addWidget(self.radioIm1, 1, 0)
        grid.addWidget(self.radioIm2, 2, 0)
        grid.addWidget(self.radioIm3, 1, 1)
        grid.addWidget(self.radioIm4, 2, 1)

        grid.addWidget(buttonBox, 2, 3)

        self.setLayout(grid)

        # connect the buttons to the apply and close slots

        self.connect(buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))

        self.connect(self.radioIm1, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.radioIm2, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.radioIm3, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.radioIm4, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)

        # set the dialog position and size based on the last open session
        # diffrent for python 3 and 2, so make two version of it
        settings = QtCore.QSettings()
        if version_info[0] == 3:
            self.restoreGeometry(settings.value("ImagePlotDlg/Geometry",
                                                QtCore.QByteArray()))
        else:
            size = settings.value("ImagePlotDlg/Size",
                                  QtCore.QVariant(QtCore.QSize(800, 1000))).toSize()
            self.resize(size)
            position = settings.value("ImagePlotDlg/Position",
                                      QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
            self.move(position)

        self.setWindowTitle("Camera View")

    def radioToggled(self, set):

        if set:
            self.updateCCDImage(self.radar)

    def closeEvent(self, event):


        # before closing the window store its size and position
        settings = QtCore.QSettings()
        if version_info[0] == 3:
            settings.setValue("ImagePlotDlg/Geometry", self.saveGeometry())
        else:
            settings.setValue("ImagePlotDlg/Size", QtCore.QVariant(self.size()))
            settings.setValue("ImagePlotDlg/Position",
                              QtCore.QVariant(self.pos()))

        self.emit(QtCore.SIGNAL("closedImage"))

    def updateCCDImage(self, radar):

        self.logger.debug("updating the ccd image")

        if self.radioIm1.isChecked():
            self.ccd_image.setImage(np.nan_to_num(radar.camera.ccd_wave_height[:, ::-1]))
        elif self.radioIm2.isChecked():
            self.ccd_image.setImage(np.nan_to_num(radar.camera.ccd_radar_flux[:, ::-1]))
        elif self.radioIm3.isChecked():
            self.ccd_image.setImage(np.nan_to_num(radar.camera.ccd_image[:, ::-1]))
        elif self.radioIm4.isChecked():
            self.ccd_image.setImage(radar.camera.ccd_isset[:, ::-1])
        else:
            self.logger.warning("radio option not recognised")