# -*- coding: utf-8 -*-
"""
Created on Wed Feb 18 11:33:31 2015

@author: eelcovv
"""

import logging
import math

from PyQt4 import QtGui, QtCore
import numpy as np
from scipy.interpolate import interp2d,interp1d

from transforms3d import taitbryan
from transforms3d import affines

import HMC.Marine.Models.CoordinateTransforms as acf
from HMC.Tools.miscellaneous import Timer


import numpy.linalg as LA

from meshArea import meshArea

from scipy.interpolate import RectBivariateSpline

class Camera(object):
    """
    class to hold all properties of a camera and to calculate pixel coordinates
    from world coordinates and vice versa
    """

    def __init__(self,
                 rotate=[0, 0, 0],
                 translate=[0, 0, 0],
                 viewangledegrees=67.38018013506,
                 aspect=4 / 3.0,
                 nxpixels=640,
                 leftHandedAxis=False,
                 logger=None):

        self.logger = logger or logging.getLogger(__name__)

        # camera position is controlled by a translate and rotate with respect
        # to the scene origin
        self.rotate = rotate
        self.translate = translate

        self.location = [0, 0, 0]

        # the camera orientation is fixed according to the POV-ray details.
        # in order to look at a different direction, define the rotation
        self.leftHandedAxis = leftHandedAxis

        # the view direction. Normally you look into the positive y axis direction, z is up, x is right
        self.direction = np.array([0, 1, 0])

        if self.leftHandedAxis:
            # the povray definition
            self.direction = np.array([0, 0, 1])

        self.focal_lenght = LA.norm(self.direction)
        self.viewangle = math.radians(viewangledegrees)
        self.aspect = aspect
        self.image_width = nxpixels

        self.update_camera_fov()

        self.update_camera_ccd()

        # not yet defined
        self.look_at = None
        self.corner_bottom_right = None
        self.corner_top_right = None
        self.corner_bottom_left = None
        self.corner_top_left = None

        self.X_world_mapping_of_corners = None
        self.X_world_mapping_of_screen = None

        # set the properties derived from the input
        self.update_camera()

    # @profile
    def update_camera(self):
        self.update_camera_extrinsic_matrix()
        self.update_camera_intrinsic_matrix()

        # also calculate the inversion matrix belong to a plane if the plane coefficients are given.
        try:
            self.invert_projection_plane()
        except:
            self.logger.warning("invert_projection_plane failed. No determined state")

    def update_camera_properties(self):
        # in case any of the camera properties are changed, run these to routine to update all its properites
        self.update_camera_fov()
        self.update_camera_ccd()

    def update_camera_fov(self):
        self.width = 2 * self.focal_lenght * math.tan(self.viewangle / 2.0)
        self.height = self.width / self.aspect
        self.logger.debug("updated camera fov : width = {} and height = {} and view angle = {}".format(
            self.width, self.height, self.viewangle))

        # for turning the camera into the z-axis direction (as required for the
        # projection algorithm) not only you need to apply the rotation given by
        # rotation, but also the rotation to turn the direction vector into z-axis
        # The RDirection matrix is used in the update_camera_extrinsic_matrix function
        # multiply the rotation matrix with

        if self.leftHandedAxis:
            # the povray definition
            self.logger.debug("setting left handed axis")
            self.up = self.height * np.array([0, 1, 0])
            self.right = self.width * np.array([1, 0, 0])
            self.sky = np.array([0, 1, 0])
            self._RDirection = np.eye(3)
            self.linear_plane_equation_coefficients = np.array([0, 1, 0, 0])
        else:
            self.logger.debug("setting right handed axis")
            # normal right handed system with z up
            self.up = self.height * np.array([0, 0, 1])
            self.right = self.width * np.array([1, 0, 0])
            self.sky = np.array([0, 0, 1])
            R_z_to_y_axis = np.array([[1, 0, 0],
                                      [0, 0, 1],
                                      [0, -1, 0]])

            self._RDirection = R_z_to_y_axis
            # this defines the horizontal ground by the equation a*x+b*y+c*z+d=0. Default is the z=0 plane: c=1, a=b=d=0
            self.linear_plane_equation_coefficients = np.array([0, 0, 1, 0])

    def update_camera_ccd(self):
        # the the image height and ccd arrays based on the image width and aspect ratio
        self.image_height = int(float(self.image_width) / self.aspect)

        self.ccd_image = np.zeros([self.image_width, self.image_height])
        self.ccd_radar_distance = np.zeros([self.image_width, self.image_height])
        self.ccd_isset = np.full([self.image_width, self.image_height], 0, dtype=int)

    # @profile
    def world_to_screen_projection(self, Xworld):
        """
        Project world coordinate vector X to the image screen

        Parameters
        ---------------------
        Xworld            : 3-D vector of the point to projet

        Returns
        ---------------------
        x_project_screen  : 2-D pixel indices u,v of the projeced point

        Examples

        xproj = world_to_screen_projection(world_vector)

        """

        # turn the world coordinate into a Homogeneous coordinate Xh=[X.T,1].T
        X_world_homogeneous = np.concatenate((Xworld, np.array([1])))

        # multiply the homogenous world coordinate with the finite projective
        # camera matrix P. Results in the homogeneous pixel vector (x,y,z)
        x_projection_screen = np.dot(self._P, X_world_homogeneous)

        # the normal pixel array is obtained by dividing by the z coordinate
        x_projection_screen /= x_projection_screen[2]

        pixels=np.array([int(np.round(x_projection_screen[0])),int(round(x_projection_screen[1]))])

        return pixels

    # @profile
    def update_camera_extrinsic_matrix(self):
        """
        calculate the extrinsic affine projection matrix. In this model you
        translate the camera to the origin and rotate the world around such
        that the camera is looking into the positive z-direction
        """
        R_180_around_z_axis = np.array([[-1, 0, 0],
                                        [0, -1, 0],
                                        [0, 0, 1]])
        self._Rvector = np.deg2rad(self.rotate)
        (rx, ry, rz) = tuple(self._Rvector)
        # turn the rotation vector in a R matrix, euler2mat  needs rz,ry,rx as input

        self._R = np.dot(self._RDirection, taitbryan.euler2mat(rz, ry, rx))

        if not self.leftHandedAxis:
            # for the right handed system the coordinates should ne rotated
            # 180 around the z-axis
            self._R = np.dot(self._R, R_180_around_z_axis)
        # the translation vector T in order to move the camera to the origin
        # because of the definition of transforms3d.affines.comopose you need to rotate already
        self._T = -np.dot(self._R, np.asarray(self.translate))

        # Now create the exentric camera matrix E = [[R -T]] [0 1]] where
        # here for T=R dot Cw and Cw is the position 'translate'of the camera
        self._extrinsicMatrix = affines.compose(self._T, self._R, np.ones(3))

    # @profile
    def update_camera_intrinsic_matrix(self):
        # update the intrinsic camera projection matrix
        if self.leftHandedAxis:
            sign = 1
        else:
            # for the right handed coordinate system the pixel array
            # should be mirrorred over the x-axis
            sign = -1
        # magnification m as pixels/m in widht an height direction
        # the (0,0) pixel is defined top, left, the (nxmax,nymax) pixel
        # bottom right. Hence we need a - sign for the y magnification
        self._magnification = np.array([sign * self.image_width / self.width,
                                        -self.image_height / self.height])
        self._alpha = self._magnification * self.focal_lenght

        # the pixel offset to turn pixel location with itsimage_width= origin at the centre
        # the the top left corner
        self._pxl = np.array([self.image_width / 2, self.image_height / 2])
        self._K = np.diag([self._alpha[0], self._alpha[1], 1])
        self._K[0:2, 2] = self._pxl[:]

        self._intrinsicMatrix = np.c_[self._K, np.zeros(3)]
        self._P = np.dot(self._intrinsicMatrix, self._extrinsicMatrix)

    # @profile
    def invert_projection_plane(self):
        """
        In order to calculate the world coordinates from the projected u,v
        pixel coordinate we need a inverse of the affine projection matrix
        P (a 3 x 4 matrix) which needs to be extended with one more equation
        describing the image_width=
        """
        self._affine_P_plane = np.vstack((self._P, self.linear_plane_equation_coefficients))
        try:
            self._inv_affine_P_plane = LA.inv(self._affine_P_plane)
        except:
            self._inv_affine_P_plane = None
            self.logger.warning("Could not invert affine project plane")

    # @profile
    def screen_to_world_plane_projection(self, screen_pixels):
        """
        Project screen coordinate vector u,v to a plane defined by the
        linear_plane_equation_coefficients [a,b,c,d]image_width=
        where a*X+b*Y+c*Z+d=0

        Parameters
        ---------------------
        screen_pixels           : 2-D vector with a screen location in pixels

        Returns
        ---------------------
        X_world  : 3-D world coordinates

        Examples

        xworld = screen_to_world([100,200],[0,1,0,0])

        return the world coordinates at the plane Y=0 (x-z plane going through origin)
        belongin to the pixel location 100,200

        !! it is assumed that the inversion matrix self._inv_affine_P_plane has already been calculated
         by calling update_radar_posision

        """

        # create the homogenous pixel coordinates : (u,v,1,0)
        pix = np.zeros(4)
        pix[0:2] = screen_pixels[:]
        pix[2] = 1
        # calcultate the wolrd coordinates on the plane 'linear_plane_equation'
        # by multiplying with the inverse projection matrix
        X_world_homogenous = np.dot(self._inv_affine_P_plane, pix)

        # normalise on last coordinate to get (X,Y,Z,1)
        X_world_homogenous /= X_world_homogenous[3]

        return X_world_homogenous[0:3]

    # @profile
    def screen_to_world_all_pixels(self, refinement_factor=[1, 1],radar=None):
        """
        loop over all the screen image pixels (u,v) and calculate the projection
        which is stored in the field X_world_mapping_of_screen

        The refinement factor is used to sample the plane at a higher density than the density
        corresponding to the pixel array of the CCD camera. This is required in order to prevent
        empty (non-filled) pixels when the wave field is mapped back to the CCD camera

        input: linear_plane_equation: equation to map the pixel on given as
        aX+bY+cZ+d=0. So to map on the y-plane, take [0,1,0,0]
        """

        nd = 3
        nx = self.image_width * refinement_factor[0]
        ny = self.image_height * refinement_factor[1]
        self.X_world_mapping_of_screen = np.zeros(
            (self.image_width * refinement_factor[0],
             self.image_height * refinement_factor[1], nd))
        for j in range(ny):
            for i in range(nx):
                ii = 1.0 * i / refinement_factor[0]
                jj = 1.0 * j / refinement_factor[1]
                self.X_world_mapping_of_screen[i][j][:nd + 1] = \
                    self.screen_to_world_plane_projection([ii, jj])
            if radar and (j % 20) == 0:
                radar.emit(QtCore.SIGNAL("progressChanged"), j, ny - 20, "Projecting all pixels to world plane")

        if radar:
            radar.emit(QtCore.SIGNAL("displayFinished"))

    def screen_to_world_corner_pixels(self,maximum_radius=None):
        """
        calculate the world positions of the corner points only
        which is stored in the field X_world_mapping_of_screen

        input: linear_plane_equation: equation to map the pixel on given as
        aX+bY+cZ+d=0. So to map on the y-plane, take [0,1,0,0]

        the corners are stored in an 2x2 array where

        vector_to_near_left  = self.X_world_mapping_of_corners[0,1]
        vector_to_near_right = self.X_world_mapping_of_corners[1,1]
        vector_to_far_left   = self.X_world_mapping_of_corners[0,0]
        vector_to_far_right  = self.X_world_mapping_of_corners[1,0]

        """

        nd = 3
        self.X_world_mapping_of_corners = np.zeros((2, 2, nd))
        for j, py in enumerate([0, self.image_height-1]):
            for i, px in enumerate([0, self.image_width-1]):
                self.X_world_mapping_of_corners[i][j][:nd + 1] = \
                    self.screen_to_world_plane_projection([px, py])

        if maximum_radius:
            # if the maximum radius is given as a parameter, set the outer corners point at the maximum in case
            # the corners points are outside the range
            normal_to_near_left=self.X_world_mapping_of_corners[0,1]/LA.norm(self.X_world_mapping_of_corners[0,1])
            normal_to_near_right=self.X_world_mapping_of_corners[1,1]/LA.norm(self.X_world_mapping_of_corners[1,1])
            if np.dot(self.X_world_mapping_of_corners[0,1],self.X_world_mapping_of_corners[0,0])<0 or \
                LA.norm(self.X_world_mapping_of_corners[0,0])> maximum_radius:
                # if the vector product between the near and far left corner is negative, it means that the
                # far corner is at the opposite side as the near corner which happens if the fov get above the horizon
                # in that case, set the far corners at the outer radius. Do the same if the corners is at the same side
                # but further away than the maximum radius

                self.X_world_mapping_of_corners[0,0]=maximum_radius*normal_to_near_left
                self.X_world_mapping_of_corners[1,0]=maximum_radius*normal_to_near_right

        # also get the look at position
        nxover2 = self.image_width / 2
        nyover2 = self.image_height / 2
        self.look_at = self.screen_to_world_plane_projection([nxover2, nyover2])

        # the vector from the radar to the look at position (hence looking down with a inclination
        self.look_at_to_radar_vector = self.translate - self.look_at
        self.look_at_to_radar_norm = LA.norm(self.look_at_to_radar_vector)
        self.look_at_to_radar_normal_vector = self.look_at_to_radar_vector / self.look_at_to_radar_norm

        # the same unity vector projected on the z plane (hence the vector over the ground looking from the
        # radar to the look_at position
        self.look_at_to_radar_base_normal_vector = np.array([self.look_at_to_radar_normal_vector[0],
                                                             self.look_at_to_radar_normal_vector[1], 0])
        self.look_at_to_radar_base_normal_vector /= LA.norm(self.look_at_to_radar_base_normal_vector)

    def properties(self):
        # purpose: return all properties of the class in one dictionary, but
        # exclude the classes
        return dict((key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def __str__(self):
        # list of the propery fields and its values of this class
        nicelist = ""
        for k, v in sorted(self.properties().items()):
            nicelist += "{:<18} : {}\n".format(k, v)
        return nicelist


class RadarWaveSimulator(QtCore.QObject):
    """
    class to hold all properties of a radar consisting of a camera object and
    a wave object
    """

    def __init__(self,
                 wave_field,
                 radar_rpm=24,
                 radar_position=[0, 0, 30],
                 radar_inclination=-15,
                 radar_start_azimuthal_angle=0,
                 radar_delta_azimuthal_angle=1.5,
                 radar_n_azimuthal_scans=100,
                 radar_n_azimuthal_scans_per_round=12,
                 radar_start_time=0,
                 radar_view_angle=1,
                 radar_vertical_spreading_angle=25,
                 radar_total_power=25000,
                 radar_image_aspect=.54,
                 radar_image_nxpixels=200,
                 image_refinement_factor=[1, 1],
                 radius_max=300,
                 radius_min=10,
                 wave_field_n_radii=100,
                 wave_field_n_azimuthal_angles=360,
                 fov_r_spacing=1.0,
                 plane_normal=[0, 0, 1],
                 playing=False,
                 logger=None,
                 parent=None
                 ):
        super(RadarWaveSimulator, self).__init__(parent)

        self.logger = logger or logging.getLogger(__name__)

        self.wave_field = wave_field

        self.radar_rpm = radar_rpm
        self.radar_delta_azimuthal_angle = radar_delta_azimuthal_angle
        self.radar_start_time = radar_start_time

        self.total_power = radar_total_power
        self.vertical_spreading_angle = radar_vertical_spreading_angle
        self.radar_reflection_acoeff = 0.9
        self.radar_reflection_bcoeff = 0.1

        # define the decay model of the radar intensity
        # 0: no decay, 1: cylindrical spreading, i.e. widens with the spreading angle and has I~1/r
        # 2: widens with spreading angle applied to a gaussian profile with respect to the view center line
        self.decay_model = 0

        self.radar_start_azimuthal_angle = radar_start_azimuthal_angle
        self.radar_n_azimuthal_scans = radar_n_azimuthal_scans
        self.radar_n_azimuthal_scans_per_round = radar_n_azimuthal_scans_per_round

        self.radius_max = radius_max
        self.radius_min = radius_min
        self.wave_field_n_radii = wave_field_n_radii
        self.wave_field_n_azimuthal_angles = wave_field_n_azimuthal_angles

        self.sample_r_spacing=fov_r_spacing

        self.plane_normal = plane_normal

        # append a zero to create the plane parameters vector [a b c 0]
        self.plane_parameters = plane_normal + [0]

        # the sampled received radar signal is stored in a 1D array
        self.sample_spacing = 7.5
        self.minimum_distance = 0
        self.maximum_distance = 1500
        self.radius_range = self.maximum_distance - self.minimum_distance
        self.n_radar_samples = int(self.radius_range / self.sample_spacing) + 1
        self.radar_signal_positions = np.linspace(0, self.radius_range, self.n_radar_samples)
        self.radar_signal_intensity = np.zeros(self.n_radar_samples)

        self.inclination = radar_inclination
        self._azimuthal_angle = radar_start_azimuthal_angle

        self.image_refinement_factor = image_refinement_factor

        self.playing = playing

        # initialise the camera and set the properties.
        self.camera = Camera()

        self.camera.translate = radar_position
        self.camera.aspect = radar_image_aspect
        self.camera.nxpixels = radar_image_nxpixels
        self.camera.viewangle = math.radians(radar_view_angle)

        # calculate the derived properties
        self.camera.update_camera_properties()

        # update the time properties
        self.update_time_settings()

        self.time = self.radar_start_time

        # a counter for the current time index
        self._time_index = 0


        # a counter for the current azimuthal index
        self._azimuthal_angle_index = 0

        self.radar_end_time = radar_start_time + self.total_time

        self.update_meshgrids()

        self.update_radargrids()

        self.plot_type = None

        self.ray_iteration_tolerance= 1e-6
        self.ray_solver=0

        # wave_sampling determines what algorithm to use for obtaining the wave height: exact (0), bilinear interpolation
        # (1), are spline interpolation (2)
        self.wave_sampling=False

        self.sample_mesh=meshArea(self.camera.X_world_mapping_of_corners)

        self.sample_mesh_2=meshArea(self.camera.X_world_mapping_of_corners)


    def update_time_settings(self):
        self.f_radar = self.radar_rpm / 60.
        self.T_radar = 1.0 / self.f_radar
        self.delta_t = self.T_radar / self.radar_n_azimuthal_scans_per_round
        self.radar_delta_azimuthal_angle = 360.0 / self.radar_n_azimuthal_scans_per_round
        self.total_time = (self.radar_n_azimuthal_scans - 1) * self.delta_t
        self.radar_end_time = self.radar_start_time + self.total_time


    # @profile
    def radar_scan_of_waves(self):
        """
        First, retrieve the coordinate positions in world coordinates belong to the pixel coordinates of the
        radar camera. Then, retrieve the wave height for all world coordinates and put this in the field
        scanned_area_wave_height.
        """
        self.logger.debug("Making radar scan of the waves")

        # the image shapes for the 1, 2, and 3-D vectors
        shape1dvec = self.camera.ccd_isset.shape
        shape2dvec = (self.camera.ccd_isset.shape[0], self.camera.ccd_isset.shape[1], 2)
        shape3dvec = (self.camera.ccd_isset.shape[0], self.camera.ccd_isset.shape[1], 3)

        # clear the ccd image and pixel information (containing the distances for each pixel to the world)
        self.camera.ccd_image = np.full(shape1dvec, np.nan)
        self.camera.ccd_wave_height = np.full(shape1dvec, np.nan)
        self.camera.ccd_wave_slope = np.full(shape2dvec, np.nan)
        self.camera.ccd_radar_flux = np.full(shape1dvec, np.nan)
        self.camera.ccd_radar_distance = np.zeros(self.camera.ccd_radar_distance.shape)
        self.camera.ccd_isset = np.full(self.camera.ccd_isset.shape, 0, dtype=int)
        self.camera.ccd_world_X = np.full(shape3dvec, np.nan)

        if self.ray_solver==0 or self.ray_solver==2 :

            # for the ray solve 0 and 2, the pixels need to be projected on the word plane
            with Timer(name="get_world_coordinates_of_plane") as t:
                self.get_world_coordinates_of_plane()

            self.scanned_area_xy_grid = self.camera.X_world_mapping_of_screen[..., :2]

            # switch x-y coordinate in order to change definition of cartesian_to_polar(x,y) which calculates r and t
            # based on mathematical definition of polar coordinates to marine definition
            # since the scanned_area_xy is obtained from the X_world_mapping of screen for which a reduction factor
            # applied, only every fx,fy pixel is obtained from the mesh. The resulting radii, thetas have therefore
            # the same resolution as the pixel array of the ccd camera (and generally less pixels then the scanned_area_xy)
            [fx, fy] = self.image_refinement_factor[:]
            (radii, thetas) = acf.cartesian_to_polar(
                self.scanned_area_xy_grid[::fx, ::fy, 1],
                self.scanned_area_xy_grid[::fx, ::fy, 0])
            self.scanned_area_rt_grid = np.array([radii, thetas])

            # calculate the wave height at each world position of the scanned area
            self.emit(QtCore.SIGNAL("update_statusbar_message"), "Map pixels to world position...")
            with Timer(name="wave_field.calculate_height") as t:
                self.scanned_area_wave_height = self.wave_field.calculate_height(
                    self.scanned_area_xy_grid.T, self.time).T

            # map the world cooridingate to the ccd array for 0 or 2. With 2, only the bottom half of the
            # image is mapped
            with Timer(name="scan_waves_by_mapping_world_to_pixels") as t1:
                self.scan_waves_by_mapping_world_to_pixels()

        if self.ray_solver==1 or self.ray_solver==2 :
            with Timer(name="scan_waves_by_tracing_from_the_pixels") as t2:
                self.scan_waves_by_tracing_from_the_pixels()

        if self.ray_solver==2:
            # for the combined method (where both scan_waves_by_mapping_world_to_pixels and
            # scan_waves_by_tracing_from_the_pixels are used) also give the total calculation time
            self.logger.debug("processed combined coarse and exact {} ms".format(t1.msecs+t2.msecs))

        with Timer(name="collect_radar_signal") as t3:
            self.collect_radar_signal()

        self.emit(QtCore.SIGNAL("displayFinished"))

        # copy this radar scan to the mesh grid
        self.logger.debug("copy radar field to index {}".format(self.azimuthal_angle_index))
        self.radar_intensity_field[self.azimuthal_angle_index, :] = self.radar_signal_intensity

    # @profile
    def find_nearest_wave_crossing(self, pixeluv):
        """
        :param u: pixel x coordinate
        :param v: pixel y coordinate
        :return:  world coordinate X of the first wave crossing seen by the pixel u,v
        """
        X = self.camera.screen_to_world_plane_projection(pixeluv)

        R = self.camera.translate
        N = X - self.camera.translate
        N /= LA.norm(N)

        delta_alpha = 10

        z_pos_start = self.wave_field.global_wave_extremes[1]+0.1
        alpha = (z_pos_start - R[2]) / N[2]
        P = R + alpha * N

        z_wave_xy = self.get_wave_height(P[:2])

        while delta_alpha > self.ray_iteration_tolerance:
            if P[2] < z_wave_xy:
                alpha -= delta_alpha
                delta_alpha/= 2.0

            alpha += delta_alpha
            P = R + alpha * N
            z_wave_xy = self.get_wave_height(P[:2])
            self.number_of_wave_evaluations+=1

        # check if the project pixel from vector P is equal to the input pixel pixeluv
        # this is the case, so remove the check
        # px2=self.camera.world_to_screen_projection(P)
        # if np.any(px2 != pixeluv):
        #     self.logger.warning("different project pixels: {} {}".format(pixeluv,px2))

        return P

    def get_wave_height(self,X):

        if not self.wave_sampling:
            eta = self.wave_field.calculate_height(X, self.time)
        else:
            eta = self.sample_mesh.splines(X[0],X[1])

        return eta


    # @profile
    def scan_waves_by_tracing_from_the_pixels(self):
        """
        For each pixel the closest wave is found by tracing along the line into the world
        Slower than the scan_waves_by_mapping_world_to_pixels but more accurate
        :return:
        """
        self.number_of_wave_evaluations=0
        for py in range(self.camera.image_height):
            for px in range(self.camera.image_width):
                if (not self.camera.ccd_isset[px, py] ):
                    self.camera.ccd_world_X[px, py, :] = self.find_nearest_wave_crossing([px, py])
                    self.camera.ccd_isset[px, py] = 1
                    self.camera.ccd_radar_distance[px, py] = LA.norm(
                                    self.camera.ccd_world_X[px,py]-self.camera.translate)
            if (py % 20) == 0:
                self.emit(QtCore.SIGNAL("progressChanged"), py, self.camera.image_height - 20, "finding nearest")

        self.logger.debug("number of wave evaluations: {}".format(self.number_of_wave_evaluations))

    # @profile
    def scan_waves_by_mapping_world_to_pixels(self):
        """
        Get the nearest wave for each pixel line by just calculating the mapped pixel grid
        :return:
        """
        nx = self.camera.X_world_mapping_of_screen.shape[0]
        ny = self.camera.X_world_mapping_of_screen.shape[1]
        if self.ray_solver==0:
            nystart=0
        else:
            # for the combined method (ray_solver==2), only map the bottom part of the image
            # the top part is fully done by the ray tracing
            nystart=int(round(ny/2))

        for j in range(nystart,ny):
            for i in range(nx):
                # get the position at the i,j location (x,y,z) and calculate the distance to the radar
                wave_xyz = np.array([self.scanned_area_xy_grid[i, j, 0],
                                     self.scanned_area_xy_grid[i, j, 1],
                                     self.scanned_area_wave_height[i, j]])

                # project the word coordinate to the camera ccd.
                (px, py) = self.camera.world_to_screen_projection(wave_xyz)


                # test if the pixels coordinates fall within the camera array and add them if that is true
                if ( ( 0 <= px < self.camera.image_width ) and ( 0 <= py < self.camera.image_height) ):

                    wave_to_radar_distance = self.radar_distance[i, j]

                    # only add the  point if the pixel was still unset or if the distance to the previous
                    # pixel is smaller (which means that this wave position is in front of the other one)
                    if ( not self.camera.ccd_isset[px, py] or
                             ( wave_to_radar_distance < self.camera.ccd_radar_distance[px, py]) ):
                        # set the radar distance and ij info for this pixel
                        self.camera.ccd_isset[px, py] = 1
                        self.camera.ccd_radar_distance[px, py] = wave_to_radar_distance
                        # only store the i j indices of the world coordinates. Fill in the properties at this location
                        # later once all the minimum distance are set
                        self.camera.ccd_world_X[px, py, :] = wave_xyz

            if (j % 20) == 0:
                self.emit(QtCore.SIGNAL("progressChanged"), j, ny - 20, "Mapping wave positions to pixels")

    def collect_radar_signal(self):
        """
        The wave field has been scanned so all the pixels contain the nears wave position
        Now we can calculate the emitted radar signal per pixel and store the equal distances
        in to a bin such that a one-dimensional radar_signal_intensity array is calculated
        with the signal vs distance
        :return:
        """
        # in the second loop, calculate the radar flux at all camera locations
        self.num_pix_not_set = 0

        # initialise the signal array to zero
        self.radar_signal_intensity[:] = 0
        for py in range(self.camera.image_height):
            for px in range(self.camera.image_width):
                if (self.camera.ccd_isset[px, py] ):

                    # the 3D location of the wave corresponding to the px,py pixel
                    pxyz = self.camera.ccd_world_X[px, py]

                    # the 2D vector (projected to the z plan) of the px,py pixel
                    pxy = pxyz[:2]

                    # the height of the wave at the px,py pixel world location
                    pz = pxyz[2]

                    # calculate the radar flux at the locale wave position
                    flux_in = self.calculate_incident_radar_flux(pxyz)

                    # calculate the local gradient of the wave field
                    wave_slope = self.wave_field.calculate_gradient(pxy, self.time)

                    # copy some quantities to the ccd image array
                    self.camera.ccd_radar_flux[px, py] = flux_in
                    self.camera.ccd_wave_slope[px, py, :] = wave_slope
                    self.camera.ccd_wave_height[px, py] = pz

                    intensity = self.calculate_returning_flux(pxyz, flux_in, wave_slope)

                    # calculate the final observer radar intensity of the current location belonging to px,py
                    self.camera.ccd_image[px, py] = intensity

                    # add the signal to the radar signal collector
                    r_index = int((self.n_radar_samples * self.camera.ccd_radar_distance[
                        px, py] - self.radius_min) / self.radius_range)
                    if r_index >= self.n_radar_samples:
                        r_index = self.n_radar_samples - 1
                    elif r_index < 0:
                        r_index = 0
                    self.radar_signal_intensity[r_index] += intensity

                else:

                    self.num_pix_not_set += 1

            if (py % 20) == 0:
                self.emit(QtCore.SIGNAL("progressChanged"), py, self.camera.image_height - 20,
                          "Calculating Radar Intensity")

        self.logger.debug("total pixels not set : {}".format(self.num_pix_not_set))

    # @profile
    def calculate_returning_flux(self, X, flux, gradient):
        """ this routine calculates the observed radar flux by the CCD camera"""

        # get the normal vector from the position X to the radar
        n_radar_to_X = self.camera.translate - X
        n_radar_to_X /= LA.norm(n_radar_to_X)

        normal_to_surface = np.array([-gradient[0], -gradient[1], 1])
        normal_to_surface /= LA.norm(normal_to_surface)

        theta_coeff = np.dot(normal_to_surface, n_radar_to_X)

        return flux * ( self.radar_reflection_acoeff * theta_coeff + self.radar_reflection_bcoeff )


    # @profile
    def calculate_incident_radar_flux(self, X):
        # get the radar flux at position X
        radar_flux_in = None
        if self.decay_model == 0:
            # no decay, just return the total power
            radar_flux_in = self.total_power
        else:
            # For the decay models I need the distance to the radar

            # get the vector from the radar (camera center) to the position X
            radar_to_X = X - self.camera.translate

            if self.decay_model == 1:
                # the cylindrical model assumes a decay proportional to  1/r (which means that spreading
                # only takes place over one direction and the intensity at the wave front is constant
                radar_travelled_distance = LA.norm(radar_to_X)

                radar_flux_in = self.total_power * self.camera.look_at_to_radar_norm / radar_travelled_distance

            elif self.decay_model == 2:
                # gaussian model assumes a gaussian profile with its maximum at the centre line of the look_to
                # vector and a width of the profile which spreads with 1/r

                # apply - sign to look_at_to_radar_normal in order to reverse it direction away from the radar to look_at
                radar_travelled_distance_along_centre=np.dot(-self.camera.look_at_to_radar_normal_vector,radar_to_X)

                # project this vector to the line from the radar to the look_at position
                radar_to_X_proj=radar_travelled_distance_along_centre*(-self.camera.look_at_to_radar_normal_vector)

                # get the distance from X position to the vector radar-look_at
                distance_X_to_center_line = LA.norm(radar_to_X_proj - radar_to_X)

                # calculate the width of the dispersing beam at this location
                width = radar_travelled_distance_along_centre * np.tan(self.vertical_spreading_angle / 2.0)

                # Here I define the sigma of the gaussian to be the width of the radar beam. (Could also be sigma=width/2)
                sigma = width

                radar_flux_in = self.total_power * np.exp(-distance_X_to_center_line ** 2 / (2 * sigma ** 2)) / (
                    sigma * np.sqrt(2 * np.pi))
            else:
                raise Exception("decay model not recognised")

        return radar_flux_in

    # @profile
    def get_world_coordinates_of_corners(self, refinement_factor=[1, 1]):
        """
        calculate the world coordinates at the plane (ground) belonging
        to all the pixels of the camera
        """
        self.logger.debug("Get world coordinates of corners")
        self.camera.screen_to_world_corner_pixels(self.radius_max)

    # @profile
    def get_world_coordinates_of_plane(self):
        """
        calculate the world coordinates at the plane (ground) belonging
        to all the pixels of the camera
        """
        self.logger.debug("Get world coordinates of plane")
        self.camera.screen_to_world_all_pixels(self.image_refinement_factor)
        # calculate the distance of each world coordinate
        self.radar_distance = LA.norm(
            self.camera.X_world_mapping_of_screen[..., :3] -
            self.camera.translate, axis=2)

    # @profile
    def update_global_wave_field(self):
        """
        calculate the wave field for time relative to t=0 (so add the radar_start_time to it)
        """
        self.logger.debug("Update wave field for time {}".format(self.time))

        self.wave_field.set_eta(self.cartesian_meshgrid, self.time + self.radar_start_time)

        # store the min and maximum wave height in the global_wave_extremes
        self.wave_field.global_wave_extremes=[np.min(self.wave_field.eta),np.max(self.wave_field.eta)]

    # @profile
    def update_radar_position(self):
        """
        Update the camera position and scan the wave for the current new time
        """
        self.camera.rotate = [self.inclination, 0, self.azimuthal_angle]

        self.camera.update_camera()

        # this routine calculates the world coordinaties of the field of view corners
        # so this means that the inverted matrix is also calculated for this routine
        self.get_world_coordinates_of_corners()

    @property
    def time_index(self):
        """
        return the index counter belonging to the current time step
        """
        self._time_index = int(round((self.time - self.radar_start_time) / self.delta_t))
        return self._time_index

    @property
    def azimuthal_angle_index(self):
        """
        return the index counter belonging to the current time step
        """
        time_fraction = np.modf((self.time - self.radar_start_time) / self.T_radar)[0]
        self._azimuthal_angle_index = min(self.radar_n_azimuthal_scans_per_round - 1,
                                          int(round(time_fraction * self.radar_n_azimuthal_scans_per_round)))

        return self._azimuthal_angle_index

    @property
    def azimuthal_angle(self):
        time_fraction = np.modf((self.time - self.radar_start_time) / self.T_radar)[0]
        self._azimuthal_angle = time_fraction * 360 + self.radar_start_azimuthal_angle
        return self._azimuthal_angle

    # @profile
    def update_meshgrids(self):
        # An array of radii
        self.logger.debug("update meshgrid rmin={} rmax={} nradii={} nphi={}".format(
            self.radius_min, self.radius_max, self.wave_field_n_radii, self.wave_field_n_azimuthal_angles))
        zeniths = np.linspace(self.radius_min, self.radius_max, self.wave_field_n_radii)

        # An array of angles i degrees
        azimuthal_angles = np.linspace(0, 360, self.wave_field_n_azimuthal_angles, endpoint=True)

        # create the 2D mesh with the polar coordinates (angles in radians)
        self.polar_meshgrid = np.meshgrid(zeniths, np.radians(azimuthal_angles))

        # turn the 2D polar coordinates into cartesian coordinates
        # convert the polar angle from mathematical definition (theta=0 -> x-axis and counter-clock rotation) to
        # naval definition (theta=0 is y-axis and clock-wise rotation) theta_naval=pi/2-theta
        self.cartesian_meshgrid = np.array(acf.polar_to_cartesian(
            self.polar_meshgrid[0], np.pi / 2 - self.polar_meshgrid[1]))

    # @profile
    def update_radargrids(self):

        # An array of radii to store the radar signal (at other resolution than the meshgrids above
        self.logger.debug("update meshgrid rmin={} rmax={} nradii={} nphi={}".format(
            self.radius_min, self.radius_max, self.n_radar_samples, self.radar_n_azimuthal_scans_per_round))
        zeniths = np.linspace(self.radius_min, self.radius_max, self.n_radar_samples)

        # An array of angles i degrees
        azimuthal_angles = np.linspace(0, 360, self.radar_n_azimuthal_scans_per_round, endpoint=True)

        # create the 2D mesh with the polar coordinates (angles in radians)
        self.polar_radargrid = np.meshgrid(zeniths, np.radians(azimuthal_angles))

        self.radar_intensity_field = np.zeros((self.radar_n_azimuthal_scans_per_round, self.n_radar_samples))

        # turn the 2D polar coordinates into cartesian coordinates
        # convert the polar angle from mathematical definition (theta=0 -> x-axis and counter-clock rotation) to
        # naval definition (theta=0 is y-axis and clock-wise rotation) theta_naval=pi/2-theta
        self.cartesian_radargrid = np.array(acf.polar_to_cartesian(
            self.polar_radargrid[0], np.pi / 2 - self.polar_radargrid[1]))

    # @profile
    def update_fovgrids(self):

        # create a cartesian grid to store the wave field which can be used for resampling

        # create a square sample area at the theta=0 position
        self.logger.debug("updating meshArea")

        self.logger.debug("updating meshArea wave field")
        self.update_waves_over_sample_mesh()


    # @profile
    def update_waves_over_sample_mesh(self):

        # calculate the wave field at the cartesian mesh for the current camera angle
        self.sample_mesh.rotate_mesh(self.azimuthal_angle)

        # calculate the wave height at each world position of the scanned area
        self.emit(QtCore.SIGNAL("update_statusbar_message"), "Calculate wave field over sample area...")

        self.sample_mesh.wave_field = self.wave_field.calculate_height(
            self.sample_mesh.grid_rotated,
            self.time)

        if self.wave_sampling:
            self.logger.debug("now calling construct_Wave_on_mesh with splines!!!")
            # only calculate the spline if we need it for the iterative model to determine the height
            # using interpolation (and not exact evaluation)

            with Timer(name="sample_mesh.get_splines()") as t:
                self.sample_mesh.get_splines()

    def construct_waves_on_interpolated_mesh(self):
        # calculate the wave field at the cartesian mesh for the current camera angle
        self.logger.debug("ROTATING sample_mesh_2")
        self.sample_mesh_2.rotate_mesh(self.azimuthal_angle)

         # calculate the wave height at each world position of the scanned area
        self.emit(QtCore.SIGNAL("update_statusbar_message"), "Interpolating wave field over fine sample area...")

        with Timer(name="sample_mesh.splines()") as t:
            self.sample_mesh_2.wave_field = self.sample_mesh.splines(self.sample_mesh_2.grid[0][0,:],
                                                                 self.sample_mesh_2.grid[1][:,1])
        self.emit(QtCore.SIGNAL("update_statusbar_message"), "Ready.")


    @staticmethod
    def image_formats():
        return "*.png *.jpg *.eps"

    def properties(self):
        # purpose: return all properties of the class in one dictionary, but
        # exclude the classes
        return dict((key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def __str__(self):
        # list of the propery fields and its values of this class
        nicelist = ""
        for k, v in sorted(self.properties().items()):
            nicelist += "{:<18} : {}\n".format(k, v)
        return nicelist


