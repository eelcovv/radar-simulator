from sys import version_info
import logging

from PyQt4 import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import ParameterTree


class RadarSettingDlg(QtGui.QDialog):
    def __init__(self, params, parent=None):
        super(RadarSettingDlg, self).__init__(parent)

        # initialse the logger
        self.logger = logging.getLogger(__name__)

        # make sure the dialog is close (not hidden)
        # actually, I want to hide it
        # self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.params = params

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Apply |
                                           QtGui.QDialogButtonBox.Close)

        self.treeRadarParamWidget = radarParameterTreeWidget(self.params)

        # create the dialog with the tree and the buttons below it
        grid = QtGui.QVBoxLayout()
        grid.addWidget(self.treeRadarParamWidget)
        grid.addWidget(buttonBox)

        self.setLayout(grid)

        # connect the buttons to the apply and close slots
        self.connect(buttonBox.button(QtGui.QDialogButtonBox.Apply),
                     QtCore.SIGNAL("clicked()"), self.apply)
        self.connect(buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))

        # set the dialog position and size based on the last open session
        settings = QtCore.QSettings()
        if version_info[0] == 3:
            self.restoreGeometry(settings.value("RadarSettingDlg/Geometry",
                                                QtCore.QByteArray()))
        else:
            size = settings.value("RadarSettingDlg/Size",
                                  QtCore.QVariant(QtCore.QSize(800, 1000))).toSize()
            self.resize(size)
            position = settings.value("RadarSettingDlg/Position",
                                      QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
            self.move(position)
        self.setWindowTitle("Radar Simulator")

        # TODO: restore the last state of the dialog. Does not work yet.
        # self.params.setCurrentState()

    def apply(self):

        # create a signal indicating that the settings have been changed and should be applied
        # is picked up in radarMain
        self.emit(QtCore.SIGNAL("radarParamDlgChanged"))


    def closeEvent(self, event):

        # before closing the window store its size and position
        settings = QtCore.QSettings()
        if version_info[0] == 3:
            settings.setValue("RadarSettingDlg/Geometry", self.saveGeometry())
        else:
            settings.setValue("RadarSettingDlg/Size", QtCore.QVariant(self.size()))
            settings.setValue("RadarSettingDlg/Position",
                              QtCore.QVariant(self.pos()))

        # TODO: want to store the state including exapansions. Does not work yet
        # self.params.current_state=self.treeRadarParamWidget.radarParameters.Parameters.saveState()

        self.emit(QtCore.SIGNAL("radarParamDlgClosed"))


## test add/remove
## this group includes a menu allowing the user to add new parameters into its child list
class ScalableGroup(pTypes.GroupParameter):
    def __init__(self, **opts):
        opts['type'] = 'group'
        opts['addText'] = "Add"
        opts['addList'] = ['str', 'float', 'int']
        pTypes.GroupParameter.__init__(self, **opts)

    def addNew(self, typ):
        val = {
            'str': '',
            'float': 0.0,
            'int': 0
        }[typ]
        self.addChild(
            dict(name="ScalableParam %d" % (len(self.childs) + 1), type=typ, value=val, removable=True, renamable=True))


class radarParameterTreeWidget(QtGui.QWidget):
    """
    Widget for the parameter tree and some routines to update the data
    and the x and y ranges. There as 2 subplots
    """

    def __init__(self, radarParameters, parent=None):
        QtGui.QWidget.__init__(self, parent)

        # initialse the logger
        self.logger = logging.getLogger(__name__)

        self.radarParameters = radarParameters
        self.tree = ParameterTree()
        self.tree.setParameters(self.radarParameters.Parameters, showTop=False)
        self.tree.setWindowTitle('Radar Parameters')
        # self.tree.setMinimumHeight(200)
        #self.tree.setMaximumHeight(200000)
        self.tree.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        self.layout = QtGui.QGridLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(QtGui.QLabel("Radar Parameters"), 0, 0, 1, 1)
        self.layout.addWidget(self.tree, 1, 0, 1, 1)