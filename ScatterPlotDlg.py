from sys import version_info
import logging

from PyQt4 import QtGui, QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree, ParameterItem, registerParameterType
import pyqtgraph as pg
import numpy as np


class ScatterPlotDlg(QtGui.QDialog):
    def __init__(self, radar, parent=None):
        super(ScatterPlotDlg, self).__init__(parent)

        # initialse the logger
        self.logger = logging.getLogger(__name__)

        self.radar = radar

        # make sure the dialog is close (not hidden)
        # actually, I want to hide it
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        self.radioIm1 = QtGui.QRadioButton()
        self.radioIm1.setText("Wave Gradient")
        self.radioIm1.setChecked(True)
        self.radioIm2 = QtGui.QRadioButton()
        self.radioIm2.setText("Incident Radar Flux")
        self.radioIm3 = QtGui.QRadioButton()
        self.radioIm3.setText("Reflected radar flux")
        self.radioIm4 = QtGui.QRadioButton()
        self.radioIm4.setText("Radar signal total")


        self.checkBox1 = QtGui.QCheckBox()
        self.checkBox1.setText("Include Interpolated Wave")

        # stack = QtGui.QStackedWidget()

        self.view = pg.GraphicsLayoutWidget()


        # create two plots above each other. The first contains the wave field over the scan
        self.plot1 = self.view.addPlot(name="Wave height")
        self.plot1.setTitle('Wave height vs distance')
        self.plot1.setLabel('left', "Wave Height", units='m')
        self.plot1.setLabel('bottom', "Distance", units='m')
        self.plot1.showGrid(x=True, y=False)
        self.legend1 = self.plot1.addLegend()


        # the second plot can be selected with the radio buttons. The x-axes range is linked to the first plot
        self.view.nextRow()
        self.plot2 = self.view.addPlot()
        self.plot2.setXLink("Wave height")
        self.plot2.showGrid(x=True, y=False)

        self.legend2 = self.plot2.addLegend()

        self.updateScatterPlots(radar)


        # create the dialog with the tree and the buttons below it
        grid = QtGui.QGridLayout()
        grid.addWidget(self.view, 0, 0, 1, 5)

        grid.addWidget(self.radioIm1, 1, 0)
        grid.addWidget(self.radioIm2, 2, 0)
        grid.addWidget(self.radioIm3, 1, 1)
        grid.addWidget(self.radioIm4, 2, 1)
        grid.addWidget(self.checkBox1, 1, 2)

        grid.addWidget(buttonBox, 2, 3)

        self.setLayout(grid)

        # connect the buttons to the apply and close slots

        self.connect(buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))

        self.connect(self.radioIm1, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.radioIm2, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.radioIm3, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.radioIm4, QtCore.SIGNAL("toggled(bool)"),
                     self.radioToggled)
        self.connect(self.checkBox1, QtCore.SIGNAL("toggled(bool)"),
                     self.checkToggled)

        # set the dialog position and size based on the last open session
        settings = QtCore.QSettings()
        if version_info[0] == 3:
            self.restoreGeometry(settings.value("ScatterPlotDlg/Geometry",
                                                QtCore.QByteArray()))
        else:
            size = settings.value("ScatterPlotDlg/Size",
                                  QtCore.QVariant(QtCore.QSize(800, 1000))).toSize()
            self.resize(size)
            position = settings.value("ScatterPlotDlg/Position",
                                      QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
            self.move(position)
        self.setWindowTitle("Scan Plots")

    def radioToggled(self, set):

        if set:
            self.updateScatterPlots(self.radar)

    def checkToggled(self, set):

        self.updateScatterPlots(self.radar)


    def closeEvent(self, event):


        # before closing the window store its size and position
        settings = QtCore.QSettings()
        if version_info[0] == 3:
            settings.setValue("ScatterPlotDlg/Geometry", self.saveGeometry())
        else:
            settings.setValue("ScatterPlotDlg/Size", QtCore.QVariant(self.size()))
            settings.setValue("ScatterPlotDlg/Position",
                              QtCore.QVariant(self.pos()))

        self.emit(QtCore.SIGNAL("closedScatter"))

    def updateScatterPlots(self, radar):

        # the position in the CCD image for the real image and the resample image
        nxover2_ccd_image = int(round(radar.camera.ccd_image.shape[0] / 2))
        nxover2_sample_mesh = int(round(radar.sample_mesh.nx_points/2))
        nxover2_sample_mesh_2 = int(round(radar.sample_mesh_2.nx_points/2))


        self.clear_plots_and_legends()

        # always plot the wave height vs distance on the full mesh
        dist_sample_mesh_1 = radar.sample_mesh.grid_rotated[1][:,nxover2_sample_mesh]



        # this distance give the distance a the the mesh points corresponding to the ccd pixels (so another resolution)
        dist_ccd = np.linalg.norm(radar.camera.ccd_world_X[nxover2_ccd_image, :, :2], axis=1)

        # the height of the waves over a center line
        height = radar.sample_mesh.wave_field[:,nxover2_sample_mesh].flatten()
        heightplot = self.plot1.plot(dist_sample_mesh_1, height)

        if self.checkBox1.isChecked():
            self.logger.debug("adding spline plot")
            try:
                dist_sample_mesh_2 = radar.sample_mesh_2.grid_rotated[1][:,nxover2_sample_mesh_2]
                height_2 = radar.sample_mesh_2.wave_field[nxover2_sample_mesh,:].flatten()
                heightplot_2=self.plot1.plot(dist_sample_mesh_2, height_2,pen='y')
                self.legend1.addItem(heightplot_2, "Height Spline")
            except  :
                pass

        # the heigt of the waves seen at the ccd image (so resampled)
        sp = pg.ScatterPlotItem(x=dist_ccd,
                                y=radar.camera.ccd_wave_height[nxover2_ccd_image, :],
                                size=4, pen=pg.mkPen(255, 0, 0))

        self.plot1.addItem(sp)
        self.legend1.addItem(heightplot, "Height")
        self.legend1.addItem(sp, "Visible")

        if self.radioIm1.isChecked():

            # create a plot of the slope height (visible part only by scatter points and the total mesh view)

            self.plot2.setTitle('Slope of visible waves')
            self.plot2.setLabel('left', "grad_x/grad_y eta", units='m')
            self.plot2.setLabel('bottom', "Distance", units='m')

            # the wave height of the visible part of the waves

            spx = pg.ScatterPlotItem(x=dist_ccd,
                                     y=radar.camera.ccd_wave_slope[nxover2_ccd_image, :, 0],
                                     size=4, pen=pg.mkPen(255, 0, 0), name="grad X")
            spy = pg.ScatterPlotItem(x=dist_ccd,
                                     y=radar.camera.ccd_wave_slope[nxover2_ccd_image, :, 1],
                                     size=4, pen=pg.mkPen(0, 0, 255), name="grad Y")

            # the wave height over the whole sample area (also the invisible parts
            self.plot2.addItem(spx)
            self.plot2.addItem(spy)

            self.legend2.addItem(spx, "grad X")
            self.legend2.addItem(spy, "grad Y")


        elif self.radioIm2.isChecked():

            # create a plot of the incident radar flux

            self.plot2.setTitle('Incident radar flux')
            self.plot2.setLabel('left', "flux", units='W/m2')
            self.plot2.setLabel('bottom', "Distance", units='m')

            # the wave height of the visible part of the waves

            spflux = pg.ScatterPlotItem(x=dist_ccd,
                                        y=radar.camera.ccd_radar_flux[nxover2_ccd_image, :],
                                        size=4, pen=pg.mkPen(255, 0, 0), name="grad X")

            # the wave height over the whole sample area (also the invisible parts
            self.plot2.addItem(spflux)

            self.legend2.addItem(spflux, "flux")

        elif self.radioIm3.isChecked():

            # create a plot of the incident radar flux

            self.plot2.setTitle('Emitted radar flux')
            self.plot2.setLabel('left', "flux", units='W/m2')
            self.plot2.setLabel('bottom', "Distance", units='m')

            # the wave height of the visible part of the waves

            sp = pg.ScatterPlotItem(x=dist_ccd,
                                    y=radar.camera.ccd_image[nxover2_ccd_image, :],
                                    size=4, pen=pg.mkPen(255, 0, 0))

            # the wave height over the whole sample area (also the invisible parts
            self.plot2.addItem(sp)

            # self.legend2.addItem(sp,"flux")

        elif self.radioIm4.isChecked():

            # create a plot of the incident radar flux

            self.plot2.setTitle('Radar Signal')
            self.plot2.setLabel('left', "flux", units='W')
            self.plot2.setLabel('bottom', "Distance", units='m')

            # the wave height of the visible part of the waves

            sp = pg.ScatterPlotItem(x=radar.radar_signal_positions,
                                    y=radar.radar_signal_intensity,
                                    size=4, pen=pg.mkPen(0, 255, 0))

            # the wave height over the whole sample area (also the invisible parts
            self.plot2.addItem(sp)
            # self.plot2.setLogMode(False,True)

            #  self.legend2.addItem(sp,"Signal")

        self.view.repaint()

    def clear_plots_and_legends(self):

        # clear the plots and manually remove the legend keys

        self.plot1.clear()
        self.plot2.clear()

        # clear the legend keys
        self.legend1.items = []
        while self.legend1.layout.count() > 0:
            self.legend1.layout.removeAt(0)
        self.legend2.items = []
        while self.legend2.layout.count() > 0:
            self.legend2.layout.removeAt(0)