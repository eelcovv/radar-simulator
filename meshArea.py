__author__ = 'eelcovv'

import logging

import numpy as np
from scipy.interpolate import RectBivariateSpline

import CoordinateTransforms as acf


class meshArea(object):
    """
    class to hold a area of four corners together with the sample spacing
    definition of the corners

    input: fov_corners i.e. the corners of the field of view of the camera. This fov is rotate with an angle thetas
    with respect to the y-axis.

    In this class, square area with cartesian uniform grid is calcuate at the theta=0 direction, with the
    width of the rectangle based on the top of the fov given by fov_corners. The fov corners however form a trapezium,
    the rectangle only has a top side corresponding to the trapezium.

    definition of the fov corners is
    0,0 : far left corner
    0,1 : near left corner
    1,0 : far right corner
    1,1 : near right corner
    theta = 0 : y-asix
    theta increasing in clockwise direction
    """

    def __init__(self,
                 fov_corners,
                 delta_x=10,
                 delta_y=10,
                 spline_k=[3, 3],
                 logger=None):
        self.logger = logger or logging.getLogger(__name__)

        self.spline_k = spline_k

        self.fov_corners = fov_corners

        self.delta_x = delta_x
        self.delta_y = delta_y

    def create_mesh(self, fov_corners=None):

        if fov_corners is not None:
            # overwrite the previous corners
            self.fov_corners = fov_corners

        # retrieve the radius and theta of each corner
        (fov_radii, fov_thetas) = acf.cartesian_to_polar(self.fov_corners[..., 1],
                                                         self.fov_corners[..., 0])

        fov_theta_range = fov_thetas[1, 0] - fov_thetas[0, 0]

        self.theta_half_angle = fov_theta_range / 2.0

        # create a square with its top in the direction of the y-axis (toward theta=0)
        cos_t2 = np.cos(self.theta_half_angle)
        sin_t2 = np.sin(self.theta_half_angle)
        self.xposmax = fov_radii[0, 0] * sin_t2  # the x position top right corner of the square
        self.xposmin = -self.xposmax  # the x position top left corner of the square
        self.yposmax = fov_radii[0, 0] * cos_t2  # the y position top right corner of the square
        self.yposmin = fov_radii[0, 1] * cos_t2  # the y position of the bottom right corner

        # calculate the corners of a square at the theta=zero position
        self.corners = np.array([
            [self.xposmin, self.yposmin],
            [self.xposmin, self.yposmax],
            [self.xposmax, self.yposmax],
            [self.xposmax, self.yposmin]
        ])

        self.corners_rotated = self.corners

        self.width = self.xposmax - self.xposmin
        self.height = self.yposmax - self.yposmin

        # these fields are used to store the sample data of the wave field and intensity
        self.wave_field = None
        self.intensity_field = None

        self.nx_points = max(int(round(self.width / self.delta_x)), self.spline_k[0])
        if (self.nx_points % 2) == 0:
            # force odd number of x points to get on line along the y axis
            self.nx_points += 1
        self.ny_points = max(int(round(self.height / self.delta_y)), self.spline_k[1])

        self.points_x = np.linspace(self.xposmin, self.xposmax, self.nx_points, endpoint=True)
        self.points_y = np.linspace(self.yposmin, self.yposmax, self.ny_points, endpoint=True)

        if self.nx_points > 1:
            self.delta_x = self.points_x[1] - self.points_x[0]

        if self.ny_points > 1:
            self.delta_y = self.points_y[1] - self.points_y[0]

        self.grid = np.meshgrid(self.points_x, self.points_y)

    def rotate_mesh(self, azimuthal_angle):
        "calculate the wave field at the cartesian mesh"

        XA = self.grid[0]
        YA = self.grid[1]
        # take the negative angle because I apply the mathematical definition of rotation, whereas azimuthal
        # is the marine definition (clockwise is positive)
        theta = -np.radians(azimuthal_angle)
        cs = np.cos(theta)
        sn = np.sin(theta)
        self.grid_rotated = np.array([XA * cs - YA * sn, XA * sn + YA * cs])

        self.corners_rotated = np.array([
            self.grid_rotated[:, 0, 0],
            self.grid_rotated[:, 0, -1],
            self.grid_rotated[:, -1, -1],
            self.grid_rotated[:, -1, 0]]
        )

        (radii, thetas) = acf.cartesian_to_polar(
            self.grid_rotated[1],
            self.grid_rotated[0])
        self.polar_grid_rotated = np.array([radii, thetas])

    def get_splines(self):
        # get the interpolation splines
        self.splines = RectBivariateSpline(self.points_x, self.points_y,
                                           self.wave_field.T,
                                           kx=self.spline_k[0],
                                           ky=self.spline_k[1])

    def interpolate_mesh_using_splines(self, splines):
        self.wave_field = splines(self.grid_rotated)
