# -*- coding: utf-8 -*-
"""
Created on Sat Feb  7 21:25:39 2015

@author: eelco
"""
import logging

from PyQt4 import QtGui
import numpy as np
from matplotlib import cm
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import HMC.Marine.Models.CoordinateTransforms as acf


class mplPlotCanvas(FigureCanvas):
    """
    Create the canvas containing 2 sub plots with the amplitude and phase
    """

    def __init__(self, size=(12.0, 3.0), dpi=100, logger=None):
        self.logger = logger or logging.getLogger(__name__)

        self.logger.info("Initialise matplotlib line plot")

        self._figure = Figure(size, dpi=dpi)
        gs = gridspec.GridSpec(1, 1)
        # create some extra space
        gs.update(bottom=0.2)

        self._axis = self._figure.add_subplot(gs[:])


        # handle to the line plots
        self.l_ydata = []

        self._axis.set_xlabel("Distance from radar [m]")

        self._plots = None

        self._textlabels = []

        self._fovline = None
        self._fovpoints = None

        self.set_bars_and_titles = True

        FigureCanvas.__init__(self, self._figure)
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)


class mplPlotCanvasWidget(QtGui.QWidget):
    """
    Widget for the matplotlib canvas and some routines to update the data
    and the x and y ranges. There as 2 subplots
    """

    def __init__(self, parent=None):


        QtGui.QWidget.__init__(self, parent)
        self.mplcanvas = mplPlotCanvas()
        self.mplcanvas.setParent(self)
        self.value_title = ""
        self.plot_title = ""
        self.update = True
        self.updateAxis()

        self.mpltoolbar = NavigationToolbar(self.mplcanvas, self)

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.mpltoolbar)
        self.vbox.addWidget(self.mplcanvas)

        self.setLayout(self.vbox)

    def updateAxis(self, radius_range=[0, 1000], n_ticks=3):

        self.mplcanvas._axis.set_xlim(radius_range[0], radius_range[1])
        self.mplcanvas._axis.set_ylim(-5, 5)

        self.mplcanvas._axis.set_xticks(np.linspace(radius_range[0], radius_range[1], n_ticks, endpoint=True))

    def initPlotLines(self, linenames):
        # initialise the lines of the amplitude/phase plot.
        for i in range(len(linenames)):
            line, = self.mplcanvas._axis.plot([], [], label=linenames[i])
            self.mplcanvas.l_ydata.append(line)

        self.mplcanvas._axis.legend(handles=self.mplcanvas.l_ydata, loc=1, bbox_to_anchor=(1, 1), shadow=True)


    # @profile
    def updateLinePlot(self, radar, filename=None, logger=None):
        # create and update of the polar contour plot

        self.logger = logger or logging.getLogger(__name__)

        self.logger.debug("Updating line plot")



        # set the time and angle string
        lx = 1.1
        ly = 0.95
        dy = 0.07
        labels = ["{: >8.2f}{:1s}".format(radar.time, "s"),
                  "{: >8.0f}{:1s}".format(radar.azimuthal_angle, "$^\circ$")]
        positions = [[lx, ly], [lx, ly - dy]]
        for i, label in enumerate(labels):
            self.mplcanvas._axis.text(positions[i][0], positions[i][1], label,
                                      family="monospace",
                                      transform=self.mplcanvas._axis.transAxes,
                                      fontsize=14,
                                      horizontalalignment='left')

        # draw the contour to canvas
        self.mplcanvas.acceptDrops
        self.mplcanvas.figure.canvas.draw()

        # save the contour to file
        if filename:
            self.mplcanvas.figure.savefig(filename)
