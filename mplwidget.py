# -*- coding: utf-8 -*-
"""
Created on Sat Feb  7 21:25:39 2015

@author: eelco
"""
import os
import logging

from PyQt4 import QtGui, QtCore
import numpy as np
from matplotlib import cm
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import HMC.Marine.Models.CoordinateTransforms as acf


class MplPolarPlotCanvas(FigureCanvas):
    """
    Create the canvas containing 2 sub plots with the amplitude and phase
    """

    def __init__(self, size=(4.0, 3.0), dpi=100, logger=None):
        self.logger = logger or logging.getLogger(__name__)

        self.logger.info("Initialise matplotlib figure")

        self._figure = Figure(size, dpi=dpi)
        gs = gridspec.GridSpec(1, 1)
        # create some extra space
        gs.update(right=.8, top=0.85)
        # create some extra space

        self._axis = self._figure.add_subplot(gs[:], projection='polar')
        self._axis.set_theta_zero_location("N")
        self._axis.set_theta_direction(-1)

        self._contours = None
        self._colorbar = None

        self._textlabels = []

        self._fovline = None
        self._fovpoints = None

        self._rectline = None
        self._rectpoints = None

        self.set_bars_and_titles = True

        self._colorbar_ax = self._figure.add_axes([.85, 0.35, 0.02, 0.5])

        FigureCanvas.__init__(self, self._figure)
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)


class mplPolarPlotCanvasWidget(QtGui.QWidget):
    """
    Widget for the matplotlib canvas and some routines to update the data
    and the x and y ranges. There as 2 subplots
    """

    def __init__(self, parent=None):


        QtGui.QWidget.__init__(self, parent)

        self.mplcanvas = MplPolarPlotCanvas()
        self.mplcanvas.setParent(self)
        self.update = True  # only update the plot if this is true
        self.value_title = ""
        self.plot_title = ""
        self.update = True
        self.image_suffix = ""
        self.save_every = 0
        self.contour_levels = np.linspace(-2, 2, 20, endpoint=True)
        self.colormap = cm.coolwarm
        self.updateAxis()

        self.mpltoolbar = NavigationToolbar(self.mplcanvas, self)

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.mpltoolbar)
        self.vbox.addWidget(self.mplcanvas)

        self.setLayout(self.vbox)

    def updateAxis(self, radius_range=[0, 1000], n_ticks=3):

        self.mplcanvas._axis.set_ylim(radius_range[0], radius_range[1])

        self.mplcanvas._axis.set_yticks(np.linspace(radius_range[0], radius_range[1], n_ticks, endpoint=True))

    def update_contour_bar_and_titles(self):
        self.mplcanvas._axis.set_title(self.plot_title, y=1.08)

        self.mplcanvas._colorbar = self.mplcanvas.figure.colorbar(self.mplcanvas._contours,
                                                                  cax=self.mplcanvas._colorbar_ax,
                                                                  format="%-6.1f")
        vrange = [self.contour_levels[0], self.contour_levels[-1]]
        self.mplcanvas._colorbar.set_ticks(np.linspace(vrange[0], vrange[1], 3))
        self.mplcanvas._colorbar.set_label(self.value_title)

        self.mplcanvas.set_bars_and_titles = False

    # @profile

    def updateContourPlot(self, radar,polar_mesh, plot_values, filename=None, logger=None):
        # create and update of the polar contour plot

        self.logger = logger or logging.getLogger(__name__)

        self.logger.debug("Updating contour plot inside mplwidget")


        # For a movie, clean up the old contours, text labels and line and points. Leave the axis,
        # this speeds up the drawing of the plot
        if self.mplcanvas._contours:
            # remove the contours. There are both present in the axis and contours variable. Bit tricky, but
            # this works
            while self.mplcanvas._contours.collections:
                for coll in self.mplcanvas._contours.collections:
                    self.mplcanvas._axis.collections.remove(coll)
                    self.mplcanvas._contours.collections.remove(coll)
                self.mplcanvas._contours.collections = []
                self.mplcanvas._axis.collections = []

        # also remove the text labels if present
        for i in range(len(self.mplcanvas._axis.texts)):
            self.mplcanvas._axis.texts[0].remove()

        # finally, clean up the scatter points and lines if present
        if self.mplcanvas._fovpoints is not None:
            self.mplcanvas._fovpoints = []
        if self.mplcanvas._fovline is not None:
            self.mplcanvas._fovline[0].remove()
            self.mplcanvas._fovline = []

        if self.mplcanvas._rectpoints is not None:
            self.mplcanvas._rectpoints = []
        if self.mplcanvas._rectline is not None:
            self.mplcanvas._rectline[0].remove()
            self.mplcanvas._rectline = []

        # create a new contour plot
        self.mplcanvas._contours = self.mplcanvas._axis.contourf(
            polar_mesh[1], polar_mesh[0],
            plot_values,
            self.contour_levels,
            cmap=self.colormap, linewidth=0, antialiased=False)

        # the first time, also the contour bar and title needs to be draw.
        if self.mplcanvas.set_bars_and_titles:
            self.logger.debug("updating the contours bar and title for {}".format(self.plot_title))
            self.update_contour_bar_and_titles()

        # calculate the polar coordinates of the field of view corners. Put then counter-clock direction in a list
        fovcartesian = np.array([radar.camera.look_at[:2],
                                 radar.camera.X_world_mapping_of_corners[0, 0, :2],
                                 radar.camera.X_world_mapping_of_corners[0, 1, :2],
                                 radar.camera.X_world_mapping_of_corners[1, 1, :2],
                                 radar.camera.X_world_mapping_of_corners[1, 0, :2],
                                 radar.camera.X_world_mapping_of_corners[0, 0, :2]
                                 ])
        # convert the cartesian coordinates into polar coordinates
        (fovzenith, fovthetas) = acf.cartesian_to_polar(fovcartesian.T[1], fovcartesian.T[0])

        self.mplcanvas._fovpoints = self.mplcanvas._axis.scatter(fovthetas[0:-1], fovzenith[0:-1], s=10, color='b')
        self.mplcanvas._fovline = self.mplcanvas._axis.plot(fovthetas[1:], fovzenith[1:], color=(0, 0, 1))

        # calculate the polar coordinates of the field of view corners. Put then counter-clock direction in a list
        rectcartesian = np.vstack((radar.sample_mesh.corners_rotated,radar.sample_mesh.corners_rotated[0]))

        # convert the cartesian coordinates into polar coordinates
        (rectzenith, rectthetas) = acf.cartesian_to_polar(rectcartesian.T[1], rectcartesian.T[0])

        self.mplcanvas._rectpoints = self.mplcanvas._axis.scatter(rectthetas[0:-1], rectzenith[0:-1], s=10, color='r')
        self.mplcanvas._rectline = self.mplcanvas._axis.plot(rectthetas, rectzenith, color=(1, 0, 0))

        # set the time and angle string
        lx = -0.3
        ly = 1.1
        dy = 0.07
        labels = ["{: >8.2f}{:1s}".format(radar.time, "s"),
                  "{: >8.0f}{:1s}".format(radar.azimuthal_angle, "$^\circ$")]
        positions = [[lx, ly], [lx, ly - dy]]
        for i, label in enumerate(labels):
            self.mplcanvas._axis.text(positions[i][0], positions[i][1], label,
                                      family="monospace",
                                      transform=self.mplcanvas._axis.transAxes,
                                      fontsize=14,
                                      horizontalalignment='left')

        # draw the contour to canvas
        self.mplcanvas.figure.canvas.draw()

        # save the contour to file
        if self.save_every > 0 and (radar.time_index % self.save_every) == 0:
            if filename is not None:
                # build image file name
                imfile = os.path.splitext(filename)[0] + "_" + self.image_suffix + "_t{:08.0f}".format(
                    1000 * radar.time) + ".png"

                # broadcast signal to force a status bar message
                radar.emit(QtCore.SIGNAL("update_statusbar_message"), "Writing: {}".format(imfile))

                try:
                    # write the image to file
                    self.logger.debug("write to image {}".format(imfile))
                    self.mplcanvas.figure.savefig(imfile)
                except IOError as e:
                    self.logger.warning("IO Error raised: {}".format(e))
                except:
                    self.logger.warning("Unexpected error writing image {}".format(imfile))
            elif radar.playing:
                # the configuration file was never saved to filename does not yet exit
                # and no image file name can be create. Raise the question if you want to
                # continue without saving images
                reply = QtGui.QMessageBox.warning(self,
                                                  "No image filename set",
                                                  "Continue without creating images?",
                                                  QtGui.QMessageBox.Ok | QtGui.QMessageBox.No)
                if reply == QtGui.QMessageBox.Ok:
                    self.save_every = 0
                else:
                    radar.emit(QtCore.SIGNAL("stop_playing"))

