import logging

from PyQt4 import QtGui
import numpy as np
from matplotlib import cm
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.gridspec as gridspec
from matplotlib.figure import Figure

logging.root.handlers = []
logging.basicConfig(format='[%(levelname)-7s] %(message)s', level=logging.DEBUG)

import HMC.Marine.Models.CoordinateTransforms as acf


class mplPolarContour(FigureCanvas):
    """
    Create the canvas containing 2 sub plots with the amplitude and phase
    """

    def __init__(self, title="Plot Title"):


        self._figure = Figure(figsize=(12, 8))
        gs = gridspec.GridSpec(1, 1)
        # create some extra space
        gs.update(right=.81)
        # create some extra space

        self._axis = self._figure.add_subplot(gs[:], projection='polar')
        self._axis.set_title(title, y=1.08)
        self._axis.set_theta_zero_location("N")
        self._axis.set_theta_direction(-1)
        self._axis.set_ylim(0, 1000)
        self._axis.set_yticks(np.linspace(0, 1000, 4, endpoint=True))

        self._contours = None
        self._colorbar = None

        self._textlabels = []

        self._fovline = None
        self._fovpoints = None

        self._colorbar_ax = self._figure.add_axes([.9, 0.35, 0.02, 0.5])

        FigureCanvas.__init__(self, self._figure)
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        # @profile

    def updatePlots(self, radar, filename=None):


        logging.debug("Updating the plots")

        contour_levels = np.linspace(-2, 2, 50, endpoint=True)

        # remove the contours and labels
        if self._contours:
            while self._contours.collections:
                for coll in self._contours.collections:
                    self._axis.collections.remove(coll)
                    self._contours.collections.remove(coll)
                self._contours.collections = []
                self._axis.collections = []
        # also remove the text labels if present
        for i in range(len(self._axis.texts)):
            self._axis.texts[0].remove()

        if self._fovpoints is not None:
            self._fovpoints[0].remove()
            self._fovpoints = []
        if self._fovline is not None:
            self.fovline[0].remove()
            self.fovline = []

        # creata a new contour
        self._contours = self._axis.contourf(
            radar.polar_meshgrid[1], radar.polar_meshgrid[0],
            radar.wave_field.eta,
            contour_levels,
            cmap=cm.coolwarm, linewidth=0, antialiased=False)

        # only the first time create a legend with the contour
        if self._colorbar is None:
            self._colorbar = self.figure.colorbar(self._contours,
                                                  cax=self._colorbar_ax,
                                                  format="%-6.1f"
                                                  )
            self._colorbar.set_ticks(np.linspace(-2, 2, 3))
            self._colorbar.set_label("Wave height [m]")

        fovcartesian = radar.camera.X_world_mapping_of_corners[:2]
        # fovcartesian = np.array([radar.camera.look_at[:2],
        # radar.camera.corner_bottom_left[:2],
        #                          radar.camera.corner_top_left[:2],
        #                          radar.camera.corner_top_right[:2],
        #                          radar.camera.corner_bottom_right[:2],
        #                          radar.camera.corner_bottom_left[:2]])
        # convert the cartesian coordinates into polar coordinates
        (fovzenith, fovthetas) = acf.cartesian_to_polar(fovcartesian.T[1], fovcartesian.T[0])

        self._fovpoints = ax.scatter(fovthetas[1:], fovzenith[1:], s=10, c='b')
        self._fovline = (ax.plot(fovthetas[1:], fovzenith[1:], c='b'))

        labelpos = [-0.1, 1.05]
        yspacing = 0.05
        self._axis.text(labelpos[0], labelpos[1],
                        "{:<6} : {:>5.1f}{:>2s}".format("Time", radar.time, " s"),
                        transform=self._axis.transAxes, fontsize=14, horizontalalignment='left')
        self._axis.text(labelpos[0], labelpos[1] - yspacing,
                        "{:<6} : {:>5.1f}{:>2s} ".format("Angle", radar.azimuthal_angle, "$^\circ$"),
                        transform=self._axis.transAxes, fontsize=14, horizontalalignment='left')

        self.figure.canvas.draw()
        if filename:
            self.figure.savefig(filename)
            #elf.mplcanvas.figure.canvas.repaint()

            #    self.mplcanvas.figure.canvas.draw_idle()
        logging.debug("Done")