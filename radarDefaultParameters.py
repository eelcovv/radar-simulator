import sys
import os
import logging
from re import match

import pyqtgraph as pg
import pyqtgraph.configfile
from pyqtgraph.configfile import ParseError
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter


class WaveFieldResolutionParameters(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        self.logger = logging.getLogger(__name__)

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu

        self.addChild(
            dict(name='Delta Radius', type='float', value=10, step=1, siPrefix=True, suffix='m', limits=(1, 100)))
        self.addChild(
            dict(name='Delta Theta', type='float', value=1, step=1, siPrefix=True, suffix='deg', limits=(1, 90)))
        self.addChild(dict(name='N Radii', type='int', value=100, step=10, limits=(1, 1000)))
        self.addChild(dict(name='N Angles', type='int', value=360, step=10, limits=(10, 360)))
        self.addChild(dict(name='N Points', type='int', value=36000, readonly=True))
        self.addChild(dict(name='Maximum Radius', type='float', value=1000.0, step=100.0, siPrefix=True, suffix='m',
                           limits=(200, 5000)))
        self.addChild(
            dict(name='Minimum Radius', type='float', value=0.0, step=10, siPrefix=True, suffix='m', limits=(1, 100)))

        self.delta_radius = self.param('Delta Radius')
        self.delta_theta = self.param('Delta Theta')
        self.n_radii = self.param('N Radii')
        self.n_theta = self.param('N Angles')
        self.n_points = self.param('N Points')
        self.max_radius = self.param('Maximum Radius')
        self.min_radius = self.param('Minimum Radius')

        self.delta_radius.sigValueChanged.connect(self.delta_radius_Changed)
        self.delta_theta.sigValueChanged.connect(self.delta_theta_Changed)
        self.n_radii.sigValueChanged.connect(self.n_radii_Changed)
        self.n_theta.sigValueChanged.connect(self.n_theta_Changed)
        self.min_radius.sigValueChanged.connect(self.min_radius_Changed)
        self.max_radius.sigValueChanged.connect(self.max_radius_Changed)

    def delta_radius_Changed(self):
        self.n_radii.setValue(
            int(round((self.max_radius.value() - self.min_radius.value()) / self.delta_radius.value())),
            blockSignal=self.n_radii_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())


    def delta_theta_Changed(self):
        self.n_theta.setValue(int(round(360.0 / self.delta_theta.value())),
                              blockSignal=self.n_theta_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())


    def min_radius_Changed(self):
        self.n_radii.setValue(
            int(round((self.max_radius.value() - self.min_radius.value()) / self.delta_radius.value())),
            blockSignal=self.n_radii_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())


    def max_radius_Changed(self):
        self.n_radii.setValue(
            int(round((self.max_radius.value() - self.min_radius.value()) / self.delta_radius.value())),
            blockSignal=self.n_radii_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())

    def n_radii_Changed(self):
        self.delta_radius.setValue((self.max_radius.value() - self.min_radius.value()) / self.n_radii.value(),
                                   blockSignal=self.delta_radius_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())

    def n_theta_Changed(self):
        self.delta_theta.setValue(360.0 / self.n_theta.value(),
                                  blockSignal=self.delta_theta_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())

class RayTracerParameters(pTypes.GroupParameter):
    # create a group of parameters of ray tracer
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        self.logger = logging.getLogger(__name__)

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu

        self.addChild(dict(name='Solver', type='list', values=dict(PixelMapping=0, IterativeRays=1, Combined=2), value=0))
        self.addChild(dict(name='Iteration Tolerance', type='float', value=1e-3, step=0.1, siPrefix=True, suffix='m', limits=(1e-6, 10)))
        self.addChild(dict(name='Wave Calculation', type='list', values=dict(Exact=0, Interpolation=1), value=0))
        self.addChild(dict(name='Spline kx', type='int', value=3, limits=(1, 10),siPrefix=False))
        self.addChild(dict(name='Spline ky', type='int', value=3, limits=(1, 10),siPrefix=False))
        self.addChild(dict(name='Radial Spacing Interpolation', type='float', value=10.0, limits=(0.01, 1000),siPrefix=True, suffix='m'))
        self.addChild(dict(name='Radial Spacing Reconstruction', type='float', value=1.0, limits=(0.01, 1000),siPrefix=True, suffix='m'))
        self.addChild(dict(name='Angular Spacing Interpolation', type='float', value=10.0, limits=(0.01, 1000),siPrefix=True, suffix='m'))
        self.addChild(dict(name='Angular Spacing Reconstruction', type='float', value=1.0, limits=(0.01, 1000),siPrefix=True, suffix='m'))


    def fov_radius_spacing_Changed(self):
        self.delta_theta.setValue(360.0 / self.n_theta.value(),
                                  blockSignal=self.delta_theta_Changed)
        self.n_points.setValue(self.n_theta.value() * self.n_radii.value())

class ImageParameters(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        self.logger = logging.getLogger(__name__)

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(dict(name='Inclination', type='float', value=6.75, step=0.1, siPrefix=True,
                           suffix='deg', limits=(0, 90)))
        self.addChild(dict(name='Angle Field of View', type='float', value=1, step=1, siPrefix=True,
                           suffix='deg', limits=(1, 90)))

        self.addChild(dict(name='Image Nx pixels', type='int', value=20, step=1, limits=(1, 1000)))
        self.addChild(dict(name='Image Ny pixels', type='int', value=200, step=10, limits=(1, 1000)))
        self.addChild(dict(name='Image aspect ratio', type='float', value=0.1, step=0.05, limits=(0.001, 10)))

        self.addChild(dict(name='Refinement x', type='int', value=1, limits=(1, 10)))
        self.addChild(dict(name='Refinement y', type='int', value=1, limits=(1, 10)))

        self.npx = self.param('Image Nx pixels')
        self.npy = self.param('Image Ny pixels')
        self.aspect = self.param('Image aspect ratio')

        self.npx.sigValueChanged.connect(self.npx_Changed)
        self.npy.sigValueChanged.connect(self.npy_Changed)
        self.aspect.sigValueChanged.connect(self.aspect_Changed)

    def npx_Changed(self):
        self.npy.setValue(self.npx.value() / self.aspect.value(), blockSignal=self.npy_Changed)

    def npy_Changed(self):
        self.npx.setValue(self.npy.value() * self.aspect.value(), blockSignal=self.npx_Changed)

    def aspect_Changed(self):
        self.npx.setValue(self.npy.value() * self.aspect.value(), blockSignal=self.npx_Changed)


class TemporalParameters(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        self.logger = logging.getLogger(__name__)

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(dict(name='Current Time', type='float', value=0.0, step=0.1, siPrefix=True, suffix='s'))
        self.addChild(dict(name='Current Index', type='int', value=0.0, step=1, limits=(0, None)))
        self.addChild(dict(name='Rounds per Minute', type='float', value=24.0, step=1.0, siPrefix=True, suffix='rpm'))
        self.addChild(
            dict(name='Period', type='float', value=2.5, step=0.1, siPrefix=True, suffix='s', limits=(0.1, 10)))
        self.addChild(dict(name='Start Time', type='float', value=0.0, step=10, limits=(0, None)))
        self.addChild(dict(name='End Time', type='float', value=20.8333333333, step=10, limits=(1, None)))
        self.addChild(
            dict(name='Time Step', type='float', value=0.2083333333, step=0.0001, limits=(0.0001, None), siPrefix=True,
                 suffix='s'))
        self.addChild(dict(name='Delta Azimuthal Angle', type='float', value=30.0, step=10, limits=(1, None)))
        self.addChild(dict(name='Number of Azimuthal scans', type='int', value=100, step=10, limits=(1, None)))
        self.addChild(dict(name='Number of scans per round', type='int', value=12.0, limits=(4, 1000)))

        # create some parameter variables of the members
        self.current_time = self.param('Current Time')
        self.current_index = self.param('Current Index')
        self.rpm = self.param('Rounds per Minute')
        self.T_radar_period = self.param('Period')
        self.t_start = self.param('Start Time')
        self.t_end = self.param('End Time')
        self.delta_t = self.param('Time Step')
        self.delta_angle = self.param('Delta Azimuthal Angle')
        self.number_of_steps = self.param('Number of Azimuthal scans')
        self.number_of_steps_per_round = self.param('Number of scans per round')

        # set of the signals of all values
        self.current_time.sigValueChanged.connect(self.current_time_Changed)
        self.current_index.sigValueChanged.connect(self.current_index_Changed)
        self.rpm.sigValueChanged.connect(self.rpm_Changed)
        self.T_radar_period.sigValueChanged.connect(self.T_radar_period_Changed)
        self.t_start.sigValueChanged.connect(self.t_start_Changed)
        self.t_end.sigValueChanged.connect(self.t_end_Changed)
        self.delta_t.sigValueChanged.connect(self.delta_t_Changed)
        self.delta_angle.sigValueChanged.connect(self.delta_angle_Changed)
        self.number_of_steps.sigValueChanged.connect(self.number_of_steps_Changed)
        self.number_of_steps_per_round.sigValueChanged.connect(self.number_of_steps_per_round_Changed)


    # the routines which control the mutual dependency
    def current_time_Changed(self):
        self.current_index.setValue(
            int(round((self.current_time.value() - self.t_start.value()) / self.delta_t.value())),
            blockSignal=self.current_index_Changed)

    def current_index_Changed(self):
        self.current_time.setValue(self.current_index.value() * self.delta_t.value() + self.t_start.value(),
                                   blockSignal=self.current_time_Changed)

    def number_of_steps_Changed(self):
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)

    def number_of_steps_per_round_Changed(self):
        self.delta_angle.setValue(360.0 / self.number_of_steps_per_round.value(),
                                  blockSignal=self.delta_angle_Changed)
        self.delta_t.setValue(self.T_radar_period.value() / self.number_of_steps_per_round.value(),
                              blockSignal=self.delta_t_Changed)
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)

    def rpm_Changed(self):
        self.T_radar_period.setValue(60.0 / self.rpm.value(), blockSignal=self.T_radar_period_Changed)
        self.delta_t.setValue(self.T_radar_period.value() / self.number_of_steps_per_round.value(),
                              blockSignal=self.delta_t_Changed)
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)

    def T_radar_period_Changed(self):
        self.rpm.setValue(60.0 / self.T_radar_period.value(), blockSignal=self.rpm_Changed)
        self.delta_t.setValue(self.T_radar_period.value() / self.number_of_steps_per_round.value(),
                              blockSignal=self.delta_t_Changed)
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)

    def delta_angle_Changed(self):
        self.number_of_steps_per_round.setValue(int(round(360.0 / self.delta_angle.value())))
        self.delta_t.setValue(self.T_radar_period.value() / self.number_of_steps_per_round.value(),
                              blockSignal=self.delta_t_Changed)
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)

    def delta_t_Changed(self):
        self.number_of_steps_per_round.setValue(int(round(self.T_radar_period.value() / self.delta_t.value())))
        self.delta_angle.setValue(360.0 / self.number_of_steps_per_round.value(),
                                  blockSignal=self.delta_angle_Changed)
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)
        self.current_index.setValue(
            int(round((self.current_time.value() - self.t_start.value()) / self.delta_t.value())),
            blockSignal=self.current_index_Changed)

    def number_of_steps_Changed(self):
        self.t_end.setValue(self.t_start.value() + self.number_of_steps.value() * self.delta_t.value(),
                            blockSignal=self.t_end_Changed)

    def t_end_Changed(self):
        self.number_of_steps.setValue((self.t_end.value() - self.t_start.value()) / self.delta_t.value(),
                                      blockSignal=self.number_of_steps_Changed)

    def t_start_Changed(self):
        self.number_of_steps.setValue((self.t_end.value() - self.t_start.value()) / self.delta_t.value(),
                                      blockSignal=self.number_of_steps_Changed)
        self.current_index.setValue(
            int(round((self.current_time.value() - self.t_start.value()) / self.delta_t.value())),
            blockSignal=self.current_index_Changed)


# create the radar data containing all data arrays for the camera and wavefield
class RadarParameters(object):
    def __init__(self, transfer_parameters=None):

        # initialse the logger
        self.logger = logging.getLogger(__name__)

        self.name = 'RadarParameters'

        # just the definition of the default parameters of the radar model
        params = [
            dict(name='Radar', type='group', children=[
                dict(name='Height', type='float', value=30.0, step=1.0, siPrefix=True, suffix='m'),
                dict(name='Vertical Spreading Angle', type='float', value=25, step=1.0, siPrefix=True, suffix='deg'),
                dict(name='Decay Model', type='list', values=dict(Constant=0, Cylindrical=1, Gaussian=2), value=0),
                dict(name='Reflection Coefficient a', type='float', value=0.9, step=0.1, siPrefix=False, limits=(0, 2)),
                dict(name='Reflection Coefficient b', type='float', value=0.1, step=0.1, siPrefix=False, limits=(0, 2)),
                dict(name='Total Emitted Power', type='float', value=25000, step=1000.0, siPrefix=True, suffix='W'),
                dict(name='Sample Spacing', type='float', value=7.5, step=1, siPrefix=True, suffix='m'),
                dict(name='Minimum Distance', type='float', value=100, step=10, siPrefix=True, suffix='m'),
                dict(name='Maximum Distance', type='float', value=2000, step=100, siPrefix=True, suffix='m'),
                TemporalParameters(name='Temporal'),
                ImageParameters(name='Image Properties'),
                RayTracerParameters(name="Ray Tracer")

            ]),
            dict(name='Wave Field', type='group', children=[
                dict(name='Model', type='list', values=dict(Linear=1, secondorder=2, Jonswap=3), value=1),
                dict(name='Linear Wave', type='group', children=[
                    dict(name='Amplitude', type='float', value=2.0, step=0.1, siPrefix=True, suffix='m',
                         limits=(0.1, 30)),
                    dict(name='Period', type='float', value=10.0, step=1, siPrefix=True, suffix='s',
                         limits=(0.1, 1000)),
                    dict(name='Length', type='float', value=100.0, step=10, siPrefix=True, suffix='m',
                         limits=(0.1, 1000)),
                    dict(name='Theta Direction', type='float', value=0.0, step=10, siPrefix=True, suffix='deg',
                         limits=(0., 360)),
                    dict(name='Phi Phase Shift', type='float', value=0.0, step=10, siPrefix=True, suffix='deg',
                         limits=(0., 360))
                ]),
                WaveFieldResolutionParameters(name='Resolution')
            ]),
            dict(name='Plots', type='group', children=[
                dict(name='Wave Field', type='group', children=[
                    dict(name='Update', type='bool', value=True),
                    dict(name='Save image every', type='int', value=0, limits=(0, 1000)),
                    dict(name='Image Suffix', type='str', value="waves"),
                    dict(name='Radius Minimum', type='float', value=0.0, step=10, siPrefix=True, suffix='m',
                         limits=(0, 200)),
                    dict(name='Radius Maximum', type='float', value=1000.0, step=100, siPrefix=True, suffix='m',
                         limits=(0, 5000)),
                    dict(name='N Radius Ticks', type='int', value=3, limits=(0, 10)),
                    dict(name='Contour Minimum', type='float', value=-2.0, step=0.1, siPrefix=True, suffix='m',
                         limits=(-100, 0)),
                    dict(name='Contour Maximum', type='float', value=2.0, step=0.1, siPrefix=True, suffix='m',
                         limits=(0, 100)),
                    dict(name='N Contour Levels', type='int', value=20, limits=(1, 100)),
                    dict(name='Contour Color Map', type='list', values=dict(CoolWarm=0, RainBow=1, LongRainBow=2,Greens=3,Grays=4), value=0)
                ]),
                dict(name='Secondary Field', type='group', children=[
                    dict(name='Update', type='bool', value=True),
                    dict(name='Save image every', type='int', value=0, limits=(0, 1000)),
                    dict(name='Show Plot', type='list', values=dict(Radar=0, SampledWaveField=1), value=0),
                    dict(name='Radar Field', type='group', children=[
                        dict(name='Image Suffix', type='str', value="radar"),
                        dict(name='Radius Minimum', type='float', value=0.0, step=10, siPrefix=True, suffix='m',
                             limits=(0, 200)),
                        dict(name='Radius Maximum', type='float', value=1000.0, step=100, siPrefix=True, suffix='m',
                             limits=(0, 5000)),
                        dict(name='N Radius Ticks', type='int', value=3, limits=(0, 10)),
                        dict(name='Contour Minimum', type='float', value=0.0, step=0.1, siPrefix=True,
                             limits=(0, 1000)),
                        dict(name='Contour Maximum', type='float', value=100.0, step=10, siPrefix=True,
                             limits=(0, None)),
                        dict(name='N Contour Levels', type='int', value=20, limits=(1, 100)),
                        dict(name='Contour Color Map', type='list', values=dict(CoolWarm=0, Rainbow=1, LongRainbow=2,Greens=3,Grays=4), value=2)
                    ]),
                    dict(name='Sampled Wave Field', type='group', children=[
                        dict(name='Plot Type', type='list', values=dict(Original=0, Splines=1), value=0),
                        dict(name='Image Suffix', type='str', value="sampled_waves"),
                        dict(name='Radius Minimum', type='float', value=0.0, step=10, siPrefix=True, suffix='m',
                             limits=(0, 200)),
                        dict(name='Radius Maximum', type='float', value=1000.0, step=100, siPrefix=True, suffix='m',
                             limits=(0, 5000)),
                        dict(name='N Radius Ticks', type='int', value=3, limits=(0, 10)),
                        dict(name='Contour Minimum', type='float', value=-2.0, step=0.1, siPrefix=True, suffix='m',
                             limits=(-100, 0)),
                        dict(name='Contour Maximum', type='float', value=2.0, step=0.1, siPrefix=True, suffix='m',
                             limits=(0, 100)),
                        dict(name='N Contour Levels', type='int', value=20, limits=(1, 100)),
                        dict(name='Contour Color Map', type='list', values=dict(CoolWarm=0, RainBow=1, LongRainBow=2,Greens=3,Grays=4), value=0)
                    ]),
                ]),
                dict(name='Size', type='list', values=dict(Normal=1, Small=2), value=1),
            ]),
            dict(name='Restore functionality', type='group', children=[
                dict(name='Reset to Default State', type='action'),
            ])
        ]

        self.logger.debug("Initialising the radar parameters")

        self.Parameters = Parameter.create(name=self.name, type='group', children=params)

        # store the default state settings in the field default_state
        self.defaults_state = self.Parameters.saveState()

        self.current_state = self.defaults_state

    def connect_the_whole_family(self, children, event):
        # recursively connect all the children, childeren of children, etc to the event
        for child in children:
            child.sigValueChanging.connect(event)
            self.connect_the_whole_family(child, event)

    def connect_signals_to_slots(self):

        # first connect all children, grandchilderen etc to the valueChanging event
        self.connect_the_whole_family(self.Parameters.children(), self.valueChanging)

        # conncect the global tree change to the change event
        self.Parameters.sigTreeStateChanged.connect(self.changeTreeState)

        # connect the Restore to Default State button to its method
        self.Parameters.param('Restore functionality',
                              'Reset to Default State').sigActivated.connect(self.restoreDefaultState)

    def saveConfiguration(self, fn):
        try:
            state = self.Parameters.saveState()
            pg.configfile.writeConfigFile(state, fn)
        except IOError as e:
            raise Exception("saveConfiguration failed with I/O Error({}): {}".format(e.errno, e.strerror))
        except ValueError as e:
            raise Exception("saveConfiguration failed with Value Error({}): {}".format(e.errno, e.strerror))
        except:
            raise Exception("saveConfiguration failed with Unidentified error")

        return True

    def loadConfiguration(self, fn):
        try:
            state = pg.configfile.readConfigFile(fn)
            self.loadState(state)
        except ParseError as e:
            raise Exception("loadConfiguration failed with ParseError {}".format(e))
        except IOError as e:
            raise Exception("loadConfiguration failed with IOerror ({}) : {}".format(e.errno, e.strerror))
        except:
            raise Exception("loadConfiguration failed with Unknown Exception")

        return True

    def loadState(self, state):
        try:
            # self.Parameters.param(self.name).clearChildren()
            self.Parameters.restoreState(state, removeChildren=False)
            self.connect_signals_to_slots()
        except:
            raise Exception("Failed restoring state")

    def valueChanging(self, param, value):
        self.logger.debug("Value changing (not finalized): {} {}".format(param, value))

    def changeTreeState(self, param, changes):
        self.logger.debug("tree changes:")
        for param, change, data in changes:
            path = self.Parameters.childPath(param)
            if path is not None:
                childName = '.'.join(path)
            else:
                childName = param.name()
            self.logger.debug("  parameter: {}".format(childName))
            self.logger.debug("  change:    {}".format(change))
            self.logger.debug("  data:      {}".format(str(data)))
            self.logger.debug("  ----------")
        self.Parameters.emit(QtCore.SIGNAL("radar_parameter_changed"), childName)

    def save(self):
        self.state = self.Parameters.saveState()

    def restoreDefaultState(self):
        self.Parameters.restoreState(self.defaults_state, removeChildren=False)

    def setCurrentState(self):
        self.Parameters.restoreState(self.current_state, removeChildren=False)


    def treeChanged(self, *args):
        self.logger.debug("signal tree changed")


