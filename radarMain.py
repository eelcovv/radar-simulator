# uncomment for python 2 only
# from __future__ import division,print_function,unicode_literals

import sys
import os
from math import radians
from re import match
import numpy as np
import logging
import logging.config

import pyqtgraph as pg
from PyQt4 import QtGui, QtCore
from numpy import linspace, zeros
from matplotlib import cm

from HMC.Tools.miscellaneous import initLogging
import HMC.Marine.Models.CoordinateTransforms as acf



try:
    from HMC.Tools.miscellaneous import Timer
except:
    logging.warning("Timer not loaded")

import HMC.Marine.Models.Radar as ra
import HMC.Marine.Models.WaveFields as wf

from ui_radarSimulator import Ui_RadarSimulatorMainWindow
from radarDefaultParameters import RadarParameters

import RadarSettingDlg
import ImagePlotDlg
import ScatterPlotDlg

__version__ = "0.0.1"


class RadarSimulatorMainWindow(QtGui.QMainWindow, Ui_RadarSimulatorMainWindow):
    def __init__(self, logger=None, parent=None):
        super(RadarSimulatorMainWindow, self).__init__(parent)

        # retrieve the logger for generating output to the console
        self.logger = logger or logging.getLogger(__name__)

        # create the main window GUI as defined by designmodeller
        self.setupUi(self)

        # add a permanent progress bar
        self.progress_bar = QtGui.QProgressBar(self.statusBar())
        self.statusBar().addPermanentWidget(self.progress_bar)

        # flag dirty to keep track if the status has been modified.
        self.dirty = False

        # store the configuration filename in self.filename
        self.filename = None

        # a counter to the number of processed time steps
        self.step_counter = 0

        # the setting dialog point
        self.settingsDialog = None

        # show a message in the status bar
        self.statusBar().showMessage("Initialising radar parameters")

        # read the parameter with all the default settings. Connect the signals to the fields
        self.radarParams = RadarParameters()
        self.radarParams.connect_signals_to_slots()

        # create a wave_field
        self.statusBar().showMessage("Creating wave field")
        self.wave_field = wf.Linear3D()

        # create the radar data containing all data arrays for the camera and wavefield
        self.statusBar().showMessage("Creating radar object")
        self.logger.debug("Create radar wave simulator object")
        self.radar = ra.RadarWaveSimulator(wave_field=self.wave_field)

        # set the initial plot anc contourbar titles
        self.mplWidget1.plot_title = "Wave Field"
        self.mplWidget1.value_title = "Height [m]"
        self.mplWidget2.plot_title = "Radar Field"
        self.mplWidget2.value_title = "Signal [-]"

        # this field give the plot of the secondary image: the radar field (0) or the sampled wave field (1)
        self.secondary_plot=0

        self.imagePlotDialog = None

        self.scatterPlotDialog = None

        QtCore.QObject.connect(self.action_Quit, QtCore.SIGNAL('triggered()'), self, QtCore.SLOT('close()'))

        QtCore.QObject.connect(self.action_Open, QtCore.SIGNAL('triggered()'), self.fileOpen)

        QtCore.QObject.connect(self.action_Save, QtCore.SIGNAL('triggered()'), self.fileSave)

        QtCore.QObject.connect(self.action_Save_As, QtCore.SIGNAL('triggered()'), self.fileSaveAs)

        self.connect(self.UpdatePlotsPushButton, QtCore.SIGNAL('clicked()'), self.update_plots)

        self.connect(self.PlayMoviePushButton, QtCore.SIGNAL('clicked()'), self.playMovie)

        self.connect(self.StopMoviePushButton, QtCore.SIGNAL('clicked()'), self.stopMovie)

        self.connect(self.OpenSettingsPushButton, QtCore.SIGNAL('clicked()'), self.OpenSettingsDialog)

        self.connect(self.OpenImagePushButton, QtCore.SIGNAL('clicked()'), self.OpenImagePlot)

        self.connect(self.OpenPlotsPushButton, QtCore.SIGNAL('clicked()'), self.OpenScatterPlot)

        self.connect(self.radarParams.Parameters,
                     QtCore.SIGNAL('radar_parameter_changed'), self.transfer_parameters)

        self.connect(self.radar, QtCore.SIGNAL("stop_playing"),
                     self.stopMovie)

        self.connect(self.radar, QtCore.SIGNAL("update_statusbar_message"),
                     self.show_message_in_status_bar)

        self.connect(self.radar, QtCore.SIGNAL("progressChanged"),
                     self.update_progress)
        self.connect(self.radar, QtCore.SIGNAL("displayFinished"),
                     self.hide_progress_bar)

        settings = QtCore.QSettings()
        if sys.version_info[0] == 3:
            self.recentFiles = settings.value("RecentFiles") or []
            self.restoreGeometry(settings.value("RadarSimulatorMainWindow/Geometry",
                                                QtCore.QByteArray()))
            self.restoreState(settings.value("RadarSimulatorMainWindow/State",
                                             QtCore.QByteArray()))

        else:
            self.recentFiles = settings.value("RecentFiles").toStringList()
            size = settings.value("RadarSimulatorMainWindow/Size",
                                  QtCore.QVariant(QtCore.QSize(1200, 1000))).toSize()
            self.resize(size)
            position = settings.value("RadarSimulatorMainWindow/Position",
                                      QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
            self.move(position)
            self.restoreState(
                settings.value("RadarSimulatorMainWindow/State").toByteArray())

        self.setWindowTitle("Radar Simulator")

        with pg.BusyCursor():
            self.logger.debug("load initial file")
            QtCore.QTimer.singleShot(0, self.loadInitialFile)
            self.logger.debug("Done")

            # self.connect(self.playMoviePushButton,QtCore.SIGNAL('clicked()'),self.playMovie)


    def show_message_in_status_bar(self, message=None):
        self.statusBar().showMessage(self.tr(message))

    def update_progress(self, n, nrows, message=""):
        self.progress_bar.show()
        self.progress_bar.setRange(0, nrows)
        self.progress_bar.setValue(n)
        self.statusBar().showMessage(self.tr(message))

    def hide_progress_bar(self):
        self.progress_bar.hide()
        self.statusBar().showMessage(self.tr("Ready"), 5000)

    def transfer_parameters(self, parameter=None):
        # Read all the parameter from the Parameter Tree and copy them to the radar object.
        # In case parameter is not None, the method is triggered by a value on change. Otherwise
        # the signal comes from settingsDlg

        radarR = self.radarParams.Parameters.names['Radar']

        if parameter is not None:
            self.logger.debug("received signal from {}".format(parameter))
            self.dirty = True

        self.radar.camera.translate = [0, 0, float(radarR['Height'])]

        self.radar.total_power = float(radarR['Total Emitted Power'])
        self.radar.vertical_spreading_angle = float(radarR['Vertical Spreading Angle'])
        self.radar.decay_model = int(radarR['Decay Model'])
        self.radar.radar_reflection_acoeff = float(radarR['Reflection Coefficient a'])
        self.radar.radar_reflection_bcoeff = float(radarR['Reflection Coefficient b'])

        self.sample_spacing = float(radarR['Sample Spacing'])
        self.minimum_distance = float(radarR['Minimum Distance'])
        self.maximum_distance = float(radarR['Maximum Distance'])
        self.radius_range = self.maximum_distance - self.minimum_distance
        self.n_radar_samples = int(self.radius_range / self.sample_spacing) + 1
        self.radar_signal_positions = linspace(0, self.radius_range, self.n_radar_samples)
        self.radar_signal_intensity = zeros(self.n_radar_samples)



        radarPT = radarR.names['Temporal']

        self.radar.time = float(radarPT['Current Time'])

        self.radar._time_index = float(radarPT['Current Index'])

        self.radar.delta_t = float(radarPT['Time Step'])
        self.logger.debug("detla_T: {}".format(self.radar.delta_t))
        self.radar.radar_end_time = float(radarPT['End Time'])
        self.radar.radar_start_time = float(radarPT['Start Time'])

        self.radar.radar_rpm = float(radarPT['Rounds per Minute'])
        self.radar.radar_delta_azimuthal_angle = float(radarPT['Delta Azimuthal Angle'])
        self.radar.radar_n_azimuthal_scans = int(radarPT['Number of Azimuthal scans'])
        self.radar.radar_n_azimuthal_scans_per_round=int(radarPT['Number of scans per round'])

        radarPC = radarR.names['Image Properties']
        # NOTE that the inclination is negative when looking downwards, but in the input it will read
        # it as a positive value (more intuitive), so here it is multiplied with -1
        self.radar.inclination = -float(radarPC['Inclination'])
        self.radar.camera.viewangle = radians(float(radarPC['Angle Field of View']))
        self.radar.camera.image_width = int(radarPC['Image Nx pixels'])
        self.radar.camera.aspect = float(radarPC['Image aspect ratio'])
        self.radar.image_refinement_factor = [(radarPC['Refinement x']), (radarPC['Refinement y'])]

        radarPRay = radarR.names['Ray Tracer']
        self.radar.sample_mesh.delta_y= float(radarPRay['Radial Spacing Interpolation'])
        self.radar.sample_mesh_2.delta_y = float(radarPRay['Radial Spacing Reconstruction'])
        self.radar.sample_mesh.delta_x= float(radarPRay['Angular Spacing Interpolation'])
        self.radar.sample_mesh_2.delta_x = float(radarPRay['Angular Spacing Reconstruction'])
        self.radar.ray_iteration_tolerance= float(radarPRay['Iteration Tolerance'])
        self.radar.ray_solver=int(radarPRay['Solver'])
        self.radar.wave_sampling=bool(radarPRay['Wave Calculation'])
        self.radar.sample_mesh.spline_k=[int(radarPRay['Spline kx']),int(radarPRay['Spline ky'])]



        radarW = self.radarParams.Parameters.names['Wave Field']

        model = int(radarW['Model'])

        if model == 1:
            radarWM = radarW.names['Linear Wave']
            self.radar.wave_field.A = float(radarWM['Amplitude'])
            self.radar.wave_field.period = float(radarWM['Period'])
            self.radar.wave_field.wavelength = float(radarWM['Length'])
            self.radar.wave_field.theta_direction = float(radarWM['Theta Direction'])
            self.radar.wave_field.phi_phase_shift = float(radarWM['Phi Phase Shift'])
        else:
            self.logger.warning("Model {} not yet implemented".format(model))

        radarWR = radarW.names['Resolution']
        self.radar.radius_min = float(radarWR['Minimum Radius'])
        self.radar.radius_max = float(radarWR['Maximum Radius'])
        dr = float(radarWR['Delta Radius'])
        dtheta = float(radarWR['Delta Theta'])
        self.radar.wave_field_n_radii = int((self.radar.radius_max - self.radar.radius_min) / dr)
        self.radar.wave_field_n_azimuthal_angles = int(360.0 / dtheta)

        radarP = self.radarParams.Parameters.names['Plots']
        radarP2 = radarP.names['Secondary Field']

        self.mplWidget1.update = bool(radarP.names["Wave Field"]["Update"])
        self.mplWidget1.save_every = int(radarP.names["Wave Field"]["Save image every"])
        self.mplWidget1.image_suffix = radarP.names["Wave Field"]["Image Suffix"]


        self.mplWidget2.update = bool(radarP2["Update"])
        self.mplWidget2.save_every = int(radarP2["Save image every"])

        size = radarP['Size']

        if parameter and bool(match("Plots.Size", parameter)):
            if size == 1:
                sizex = 640
                sizey = 500
            else:
                sizex = 500
                sizey = 400
            self.logger.debug("updating plot size")
            self.mplWidget1.setFixedSize(sizex, sizey)
            self.mplWidget2.setFixedSize(sizex, sizey)

        rrange = [radarP.names["Wave Field"]['Radius Minimum'],radarP.names["Wave Field"]['Radius Maximum']]
        nticks = radarP.names["Wave Field"]['N Radius Ticks']
        cmin=radarP.names["Wave Field"]["Contour Minimum"]
        cmax=radarP.names["Wave Field"]["Contour Maximum"]
        nlevels=radarP.names["Wave Field"]["N Contour Levels"]
        c_levels = linspace(cmin,cmax,nlevels,endpoint=True)
        self.mplWidget1.updateAxis(radius_range=rrange, n_ticks=nticks)
        self.mplWidget1.contour_levels = c_levels


        if radarP.names["Wave Field"]['Contour Color Map'] == 0:
            cmap=cm.coolwarm
        elif radarP.names["Wave Field"]['Contour Color Map'] == 1:
            cmap=cm.rainbow
        elif radarP.names["Wave Field"]['Contour Color Map'] == 2:
            cmap=cm.nipy_spectral
        elif radarP.names["Wave Field"]['Contour Color Map'] == 3:
            cmap=cm.Greens
        elif radarP.names["Wave Field"]['Contour Color Map'] == 4:
            cmap=cm.Greys
        self.mplWidget1.colormacmapcmapp

        if radarP2['Show Plot']==0:
            if self.secondary_plot != 0:
                self.logger.debug("switching from waves to radar")
                # the plot type changed, so reset the bars and title
                self.mplWidget2.mplcanvas.set_bars_and_titles=True
            self.secondary_plot = 0
        elif radarP2['Show Plot']==1:
            if self.secondary_plot != 1:
                self.logger.debug("switching from radar to waves")
                # the plot type changed, so reset the bars and title
                self.mplWidget2.mplcanvas.set_bars_and_titles=True
            self.secondary_plot = 1
        else:
            self.logger.warning("Second Plot should be 0 or 1!")

        if self.secondary_plot == 0:
            self.logger.debug("plottings for radar image")
            # make settings for radar field
            rrange = [radarP2.names["Radar Field"]['Radius Minimum'],radarP2.names["Radar Field"]['Radius Maximum']]
            nticks = radarP2.names["Radar Field"]['N Radius Ticks']
            cmin=radarP2.names["Radar Field"]["Contour Minimum"]
            cmax=radarP2.names["Radar Field"]["Contour Maximum"]
            nlevels=radarP2.names["Radar Field"]["N Contour Levels"]
            c_levels = linspace(cmin,cmax,nlevels,endpoint=True)
            self.mplWidget2.updateAxis(radius_range=rrange, n_ticks=nticks)
            self.mplWidget2.contour_levels = c_levels
            self.mplWidget2.plot_title = "Radar Field"
            self.mplWidget2.value_title = "Signal [-]"
            self.mplWidget2.mplcanvas.set_bars_and_titles = True

            if radarP2.names["Radar Field"]['Contour Color Map'] == 0:
                cmap=cm.coolwarm
            elif radarP2.names["Radar Field"]['Contour Color Map'] == 1:
                cmap=cm.rainbow
            elif radarP2.names["Radar Field"]['Contour Color Map'] == 2:
                cmap=cm.nipy_spectral
            elif radarP2.names["Radar Field"]['Contour Color Map'] == 3:
                cmap=cm.Greens
            elif radarP2.names["Radar Field"]['Contour Color Map'] == 4:
                cmap=cm.Greys
            self.mplWidget2.colormap= cmap
        elif self.secondary_plot==1:
            self.logger.debug("plot settings for wave plot")
            # make settings for sampled wave field
            rrange = [radarP2.names["Sampled Wave Field"]['Radius Minimum'],radarP2.names["Sampled Wave Field"]['Radius Maximum']]
            nticks = radarP2.names["Sampled Wave Field"]['N Radius Ticks']
            cmin=radarP2.names["Sampled Wave Field"]["Contour Minimum"]
            cmax=radarP2.names["Sampled Wave Field"]["Contour Maximum"]
            nlevels=radarP2.names["Sampled Wave Field"]["N Contour Levels"]
            c_levels = linspace(cmin,cmax,nlevels,endpoint=True)
            self.mplWidget2.updateAxis(radius_range=rrange, n_ticks=nticks)
            self.mplWidget2.plot_title = "Sampled Wave Field"
            self.mplWidget2.value_title = "Height [m]"
            self.mplWidget2.contour_levels = c_levels
            self.mplWidget2.mplcanvas.set_bars_and_titles = True

            if radarP2.names["Sampled Wave Field"]['Plot Type'] == 0:
                self.plot_resolution=0
            else:
                self.plot_resolution=1

            if radarP2.names["Sampled Wave Field"]['Contour Color Map'] == 0:
                cmap=cm.coolwarm
            elif radarP2.names["Sampled Wave Field"]['Contour Color Map'] == 1:
                cmap=cm.rainbow
            elif radarP2.names["Sampled Wave Field"]['Contour Color Map'] == 2:
                cmap=cm.nipy_spectral
            elif radarP2.names["Sampled Wave Field"]['Contour Color Map'] == 3:
                cmap=cm.Greens
            elif radarP2.names["Sampled Wave Field"]['Contour Color Map'] == 4:
                cmap=cm.Greys
            self.mplWidget2.colormap= cmap
        else:
            self.logger("secondary plot not recognised")

        if not parameter or \
                bool(match("Temporal..*", str(parameter))):
            self.logger.debug("call update_time_settings and TimeSpingBox from transfer ")
            self.radar.update_time_settings()

        if not parameter or \
                bool(match("Wave Field.Resolution", parameter)):
            self.logger.debug("Updating meshgrid {}".format(self.radar.wave_field_n_radii))
            self.radar.update_meshgrids()

        if not parameter or \
                bool(match("Radar.(.*Distance.*|.*Sample Spacing.*|.*Azimuthal.*)", parameter)):
            self.logger.debug("Updating radar grid {}".format(self.radar.n_radar_samples))
            self.radar.update_radargrids()


        if not parameter or \
                bool(match("Wave Field.Linear Wave.Amplitude", parameter)) or \
                bool(match("Wave Field.Linear Wave.Length", parameter)) or \
                bool(match("Wave Field.Linear Wave.Phi Phase Shift", parameter)) or \
                bool(match("Wave Field.Linear Wave.Theta Direction", parameter)):
            self.logger.debug("Updating wave field")
            self.radar.wave_field.update_wave_properties()

        if not parameter or \
                bool(match("Radar.Image Properties..*", parameter)) or \
                bool(match("Radar.Ray Tracer.*", parameter)) :
            self.logger.debug("updating camera settings")
            self.radar.camera.update_camera_properties()
            self.radar.camera.update_camera()
            self.radar.update_time_settings()
            self.radar.update_radar_position()
            self.radar.get_world_coordinates_of_corners()
            self.radar.sample_mesh.create_mesh(self.radar.camera.X_world_mapping_of_corners)
            self.radar.sample_mesh_2.create_mesh(self.radar.camera.X_world_mapping_of_corners)
            self.radar.update_fovgrids()


        if self.radar.time < self.radar.radar_start_time:
            self.logger.debug(
                "update radar time to start time : {} -> {}".format(self.radar.time, self.radar.radar_start_time))
            # The start time is higher than the current time, so set the current time to the start
            self.radar.time = self.radar.radar_start_time

    def update_plots(self):
        # update the contour plots for the current time values. Check the checkboxes to see which
        # plots need an update.
        if self.mplWidget1.update or self.mplWidget2.update:

            self.radar.update_radar_position()

            # update the global wave field
            self.radar.update_global_wave_field()


            self.radar.update_waves_over_sample_mesh()

            if self.radar.wave_sampling:
                self.radar.construct_waves_on_interpolated_mesh()

            self.logger.debug("current extremes: {} ".format(self.radar.wave_field.global_wave_extremes))

            # for the second plot, a scan of the wave is required
            self.radar.radar_scan_of_waves()

            if self.mplWidget1.update:
                # create the first plot
                self.emit(QtCore.SIGNAL("update_statusbar_message"), "Plotting Polar Wave Contour Plot...")
                self.mplWidget1.updateContourPlot(self.radar,self.radar.polar_meshgrid, self.radar.wave_field.eta,
                                                  filename=self.filename)
                self.emit(QtCore.SIGNAL("update_statusbar_message"), "Ready.")

            if self.mplWidget2.update:

                if self.secondary_plot == 0:

                    # plot the radar scan
                    self.logger.debug("plotting radar wave field")



                    self.emit(QtCore.SIGNAL("update_statusbar_message"), "Plotting Radar Contour Plot...")
                    self.mplWidget2.updateContourPlot(self.radar, self.radar.polar_radargrid,
                                                  self.radar.radar_intensity_field,
                                                  filename=self.filename)
                else :
                    # plot the wave field over the sample area to check if it is a bit ok

                    if self.radar.sample_mesh.wave_field is not None:
                        if self.plot_resolution==0:
                            # only plot the sampled original wave field
                            self.logger.debug("plotting sampled wave field")
                            self.emit(QtCore.SIGNAL("update_statusbar_message"), "Plotting Sampled Wave Area...")
                            self.mplWidget2.updateContourPlot(self.radar, self.radar.sample_mesh.polar_grid_rotated,
                                                      self.radar.sample_mesh.wave_field,
                                                      filename=self.filename)
                        else:
                            if self.radar.wave_sampling:
                                self.logger.debug("plotting splined wave field")

                                self.emit(QtCore.SIGNAL("update_statusbar_message"), "Plotting Spline Wave Area...")
                                self.mplWidget2.updateContourPlot(self.radar, self.radar.sample_mesh_2.polar_grid_rotated,
                                                          self.radar.sample_mesh_2.wave_field.T,
                                                          filename=self.filename)
                            else:
                                self.logger.warning("no spline sampling selected so field not available")
                    else:
                        self.logger.debug("sampled wave field not available")

                self.emit(QtCore.SIGNAL("update_statusbar_message"), "Ready.")



                if self.imagePlotDialog:
                    self.imagePlotDialog.updateCCDImage(self.radar)

                if self.scatterPlotDialog:
                    self.scatterPlotDialog.updateScatterPlots(self.radar)

    def stopMovie(self):
        self.playing = False

    def playMovie(self):
        self.playing = True

        if self.radar.time >= self.radar.radar_end_time:
            self.logger.warning("Radar end time already exceeded. Increase via Settings...")

        while self.radar.time < self.radar.radar_end_time:

            # check if the stop button has been pressed
            if not self.playing:
                self.logger.info("stop playing at {} s".format(self.radar.time))
                break

            # update the time step
            self.radar.time += self.radar.delta_t

            self.logger.info("Updating time step to {:.2f}".format(self.radar.time))

            self.statusBar().showMessage("Processing time step {:.2f}".format(self.radar.time))

            self.update_plots()

            # update the time counter in the main window
            # note that this also triggered a signal which triggers update_time_value. so
            # the updating of the radar position and plots is taken care of
            # self.SetTimeDoubleSpinBox.setValue(self.radar.time)

            # wait until the plot is drawn, otherwise the gui is blocked
            QtGui.QApplication.processEvents()

        self.statusBar().showMessage("Ready.")

    def CloseSettingsDialog(self):
        self.OpenSettingsPushButton.setDisabled(False)

    def OpenSettingsDialog(self):
        dialog = RadarSettingDlg.RadarSettingDlg(self.radarParams, self)
        self.connect(dialog, QtCore.SIGNAL("radarParamDlgChanged"), self.update_plots)
        self.connect(dialog, QtCore.SIGNAL("radarParamDlgClosed"), self.CloseSettingsDialog)
        self.OpenSettingsPushButton.setDisabled(True)
        dialog.show()

    def CloseImageDialog(self):
        self.OpenImagePushButton.setDisabled(False)
        self.imagePlotDialog = None

    def OpenImagePlot(self):

        self.imagePlotDialog = ImagePlotDlg.ImagePlotDlg(self.radar, self)
        self.imagePlotDialog.updateCCDImage(self.radar)
        self.connect(self.imagePlotDialog, QtCore.SIGNAL("closedImage"), self.CloseImageDialog)
        self.OpenImagePushButton.setDisabled(True)
        self.imagePlotDialog.show()

    def CloseScatterDialog(self):
        self.OpenPlotsPushButton.setDisabled(False)
        self.scatterPlotDialog = None

    def OpenScatterPlot(self):

        self.scatterPlotDialog = ScatterPlotDlg.ScatterPlotDlg(self.radar, self)
        self.scatterPlotDialog.updateScatterPlots(self.radar)
        self.connect(self.scatterPlotDialog, QtCore.SIGNAL("closedScatter"), self.CloseScatterDialog)
        self.OpenPlotsPushButton.setDisabled(True)
        self.scatterPlotDialog.show()

    def loadInitialFile(self):
        settings = QtCore.QSettings()
        if sys.version_info[0] == 3:
            fname = settings.value("LastFile")
        else:
            fname = unicode(settings.value("LastFile").toString())
        if fname and QtCore.QFile.exists(fname):
            self.logger.debug("loading {}".format(fname))
            self.statusBar().showMessage("Initialising configuration : {} ...".format(os.path.basename(fname)))
            self.loadFile(fname)
        else:
            self.logger.debug("setting initial parameters ")
            self.statusBar().showMessage("Initialising default configuration...")
            self.transfer_parameters()

        self.update_plots()
        self.statusBar().showMessage("Ready.", 5000)

    def addRecentFile(self, fname):
        if fname is None:
            return
        if sys.version_info[0] == 3:
            if fname not in self.recentFiles:
                self.recentFiles = [fname] + self.recentFiles[:8]
        else:
            if not self.recentFiles.contains(fname):
                self.recentFiles.prepend(QtCore.QString(fname))
                while self.recentFiles.count() > 9:
                    self.recentFiles.takeLast()

    def fileOpen(self):
        if not self.okToContinue():
            return
        dir = os.path.dirname(self.filename) \
            if self.filename is not None else "."
        if sys.version_info[0] == 3:
            # python 3 version
            formats = (["*.{}".format(extension.lower())
                        for extension in ["cfg"]])
            fname = QtGui.QFileDialog.getOpenFileName(self,
                                                      "Radar Simulator - Choose Configuration", dir,
                                                      "Configuration files ({})".format(" ".join(formats)))
        else:
            # python two version
            formats = ["*.%s" % unicode(extension).lower() \
                       for extension in ["cfg"]]

            fname = unicode(QtGui.QFileDialog.getOpenFileName(self,
                                                              "Radar Simulator - Choose Configuration", dir,
                                                              "Configuration files ({})".format(" ".join(formats))))

        if fname:
            self.loadFile(fname)

    def loadFile(self, fname=None):
        if fname is None:
            action = self.sender()
            if isinstance(action, QtGui.QAction):
                if sys.version_info[0]==2:
                    fname = unicode(action.data().toString())
                else:
                    fname = action.data().toString()
                if not self.okToContinue():
                    return
            else:
                return
        if fname:
            self.filename = None
            if self.radarParams.loadConfiguration(fname):
                self.logger.debug("loaded  {}".format(fname))
                self.addRecentFile(fname)
                self.filename = fname
                self.transfer_parameters()
                self.dirty = False

                message = "Loaded {}".format(os.path.basename(fname))
                self.logger.debug("{}".format(message))
            else:
                message = "Failed to load the statef file"
                self.logger.debug("{}".format(message))

            self.updateStatus(message)

    def updateStatus(self, message):
        self.statusBar().showMessage(message, 5000)
        if self.filename is not None:
            self.setWindowTitle("Radar Simulator - %s[*]" % \
                                os.path.basename(self.filename))
        else:
            self.setWindowTitle("Radar Simulator[*]")
        self.setWindowModified(self.dirty)

    def fileSave(self):
        if self.filename is None:
            self.fileSaveAs()
        else:
            if self.radarParams.saveConfiguration(self.filename):
                self.updateStatus("Saved as %s" % self.filename)
                self.dirty = False
            else:
                self.updateStatus("Failed to save %s" % self.filename)

    def fileSaveAs(self):
        fname = self.filename if self.filename is not None else "."
        if sys.version_info[0] == 3:
            formats = (["*.{}".format(format.lower())
                        for format in ["cfg"]])
            fname = QtGui.QFileDialog.getSaveFileName(self,
                                                      "Radar Simulator - Save Configuration", fname,
                                                      "Configuration files ({})".format(" ".join(formats)))

        else:
            formats = ["*.%s" % unicode(format).lower() \
                       for format in ["cfg"]]
            fname = unicode(QtGui.QFileDialog.getSaveFileName(self,
                                                              "Radar Simulator - Save Configuration", fname,
                                                              "Configuration files (%s)" % " ".join(formats)))
        if fname:
            if "." not in fname:
                fname += ".cfg"
            self.addRecentFile(fname)
            self.filename = fname
            self.fileSave()

    def okToContinue(self):
        if self.dirty:
            reply = QtGui.QMessageBox.question(self,
                                               "Radar and/or wave settings changed",
                                               "Save unsaved changes?",
                                               QtGui.QMessageBox.Yes | QtGui.QMessageBox.No |
                                               QtGui.QMessageBox.Cancel)
            if reply == QtGui.QMessageBox.Cancel:
                return False
            elif reply == QtGui.QMessageBox.Yes:
                self.fileSave()
        return True

    def closeEvent(self, event):
        if self.okToContinue():
            # store the latest state of the program
            settings = QtCore.QSettings()
            if sys.version_info[0] == 3:
                settings.setValue("LastFile", self.filename)
            else:
                filename = QtCore.QVariant(QtCore.QString(self.filename)) \
                    if self.filename is not None else QtCore.QVariant()

            if sys.version_info[0] == 3:
                settings.setValue("RecentFiles", self.recentFiles or [])
                settings.setValue("RadarSimulatorMainWindow/Geometry", self.saveGeometry())
                settings.setValue("RadarSimulatorMainWindow/State", self.saveState())
            else:
                recentFiles = QtCore.QVariant(self.recentFiles) \
                    if self.recentFiles else QtCore.QVariant()
                settings.setValue("RecentFiles", recentFiles)
                settings.setValue("RadarSimulatorMainWindow/Size", QtCore.QVariant(self.size()))
                settings.setValue("RadarSimulatorMainWindow/Position",
                                  QtCore.QVariant(self.pos()))
                settings.setValue("RadarSimulatorMainWindow/State",
                                  QtCore.QVariant(self.saveState()))
        else:
            event.ignore()


def main():
    initLogging()
    logger = logging.getLogger(__name__)
    logger.info("Start Qt Application")

    app = QtGui.QApplication(sys.argv)
    # app = pg.mkQApp()
    app.setOrganizationName("HMC Heerema Marine Contractors")
    app.setOrganizationDomain("hmc-heerema.com")
    app.setApplicationName("Radar Simulator")
    app.setWindowIcon(QtGui.QIcon(":icon.png"))
    form = RadarSimulatorMainWindow()

    form.show()
    #form.saveGeometry()
    app.exec_()


main()