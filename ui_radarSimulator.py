# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'radarSimulator.ui'
#
# Created: Tue Mar 31 17:23:25 2015
# by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_RadarSimulatorMainWindow(object):
    def setupUi(self, RadarSimulatorMainWindow):
        RadarSimulatorMainWindow.setObjectName(_fromUtf8("RadarSimulatorMainWindow"))
        RadarSimulatorMainWindow.resize(1489, 608)
        self.centralwidget = QtGui.QWidget(RadarSimulatorMainWindow)
        self.centralwidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(6, 0, 1430, 502))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.PlayMoviePushButton = QtGui.QPushButton(self.widget)
        self.PlayMoviePushButton.setObjectName(_fromUtf8("PlayMoviePushButton"))
        self.verticalLayout.addWidget(self.PlayMoviePushButton)
        self.StopMoviePushButton = QtGui.QPushButton(self.widget)
        self.StopMoviePushButton.setObjectName(_fromUtf8("StopMoviePushButton"))
        self.verticalLayout.addWidget(self.StopMoviePushButton)
        self.UpdatePlotsPushButton = QtGui.QPushButton(self.widget)
        self.UpdatePlotsPushButton.setObjectName(_fromUtf8("UpdatePlotsPushButton"))
        self.verticalLayout.addWidget(self.UpdatePlotsPushButton)
        self.OpenSettingsPushButton = QtGui.QPushButton(self.widget)
        self.OpenSettingsPushButton.setObjectName(_fromUtf8("OpenSettingsPushButton"))
        self.verticalLayout.addWidget(self.OpenSettingsPushButton)
        self.OpenImagePushButton = QtGui.QPushButton(self.widget)
        self.OpenImagePushButton.setObjectName(_fromUtf8("OpenImagePushButton"))
        self.verticalLayout.addWidget(self.OpenImagePushButton)
        self.OpenPlotsPushButton = QtGui.QPushButton(self.widget)
        self.OpenPlotsPushButton.setObjectName(_fromUtf8("OpenPlotsPushButton"))
        self.verticalLayout.addWidget(self.OpenPlotsPushButton)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)
        spacerItem1 = QtGui.QSpacerItem(18, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.mplWidget1 = mplPolarPlotCanvasWidget(self.widget)
        self.mplWidget1.setMinimumSize(QtCore.QSize(640, 500))
        self.mplWidget1.setObjectName(_fromUtf8("mplWidget1"))
        self.horizontalLayout.addWidget(self.mplWidget1)
        spacerItem2 = QtGui.QSpacerItem(13, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.mplWidget2 = mplPolarPlotCanvasWidget(self.widget)
        self.mplWidget2.setMinimumSize(QtCore.QSize(640, 500))
        self.mplWidget2.setObjectName(_fromUtf8("mplWidget2"))
        self.horizontalLayout.addWidget(self.mplWidget2)
        RadarSimulatorMainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(RadarSimulatorMainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1489, 27))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menu_File = QtGui.QMenu(self.menubar)
        self.menu_File.setObjectName(_fromUtf8("menu_File"))
        RadarSimulatorMainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(RadarSimulatorMainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        RadarSimulatorMainWindow.setStatusBar(self.statusbar)
        self.action_Open = QtGui.QAction(RadarSimulatorMainWindow)
        self.action_Open.setObjectName(_fromUtf8("action_Open"))
        self.action_Save = QtGui.QAction(RadarSimulatorMainWindow)
        self.action_Save.setObjectName(_fromUtf8("action_Save"))
        self.action_Save_As = QtGui.QAction(RadarSimulatorMainWindow)
        self.action_Save_As.setObjectName(_fromUtf8("action_Save_As"))
        self.action_Export = QtGui.QAction(RadarSimulatorMainWindow)
        self.action_Export.setObjectName(_fromUtf8("action_Export"))
        self.action_Quit = QtGui.QAction(RadarSimulatorMainWindow)
        self.action_Quit.setObjectName(_fromUtf8("action_Quit"))
        self.menu_File.addAction(self.action_Open)
        self.menu_File.addAction(self.action_Save)
        self.menu_File.addAction(self.action_Save_As)
        self.menu_File.addSeparator()
        self.menu_File.addAction(self.action_Quit)
        self.menubar.addAction(self.menu_File.menuAction())

        self.retranslateUi(RadarSimulatorMainWindow)
        QtCore.QMetaObject.connectSlotsByName(RadarSimulatorMainWindow)

    def retranslateUi(self, RadarSimulatorMainWindow):
        RadarSimulatorMainWindow.setWindowTitle(_translate("RadarSimulatorMainWindow", "MainWindow", None))
        self.PlayMoviePushButton.setText(_translate("RadarSimulatorMainWindow", "Play", None))
        self.StopMoviePushButton.setText(_translate("RadarSimulatorMainWindow", "Stop", None))
        self.UpdatePlotsPushButton.setText(_translate("RadarSimulatorMainWindow", "Update", None))
        self.OpenSettingsPushButton.setText(_translate("RadarSimulatorMainWindow", "Settings...", None))
        self.OpenImagePushButton.setText(_translate("RadarSimulatorMainWindow", "CCD Image...", None))
        self.OpenPlotsPushButton.setText(_translate("RadarSimulatorMainWindow", "Plots...", None))
        self.menu_File.setTitle(_translate("RadarSimulatorMainWindow", "&File", None))
        self.action_Open.setText(_translate("RadarSimulatorMainWindow", "&Open", None))
        self.action_Save.setText(_translate("RadarSimulatorMainWindow", "&Save", None))
        self.action_Save_As.setText(_translate("RadarSimulatorMainWindow", "Save &As", None))
        self.action_Export.setText(_translate("RadarSimulatorMainWindow", "&Export", None))
        self.action_Quit.setText(_translate("RadarSimulatorMainWindow", "&Quit", None))


from mplwidget import mplPolarPlotCanvasWidget
